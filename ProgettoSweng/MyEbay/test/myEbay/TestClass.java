package myEbay;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import myEbay.server.CategoriaDB;
import myEbay.server.Login;
import myEbay.server.MessaggiDB;
import myEbay.server.Registration;
import myEbay.shared.Categoria;
import myEbay.shared.Oggetto;
/**
 * Classe di test
 *
 * @author Andriani Elisa
 */
class TestClass {
	
	@Test
	/**
	 * Test per il Login
	 */
	@DisplayName("Test di login")
	void testLogin() {
		
		
		ArrayList<String> dati= new ArrayList<String>();
		for(int i=0; i<11; i++) {
			dati.add("utenteinfo");
		}
		//utente non trovato
		assertEquals(0, Login.doLogin("utenteprova", "pswprova") );
		//utente ha effettuato il login
		Registration.doRegistration(dati);
		assertEquals(2, Login.doLogin("utenteinfo", "utenteinfo") );
		//admin ha effettuato il login
		Registration.regAdmin();
		assertEquals(1, Login.doLogin("admin", "admin") );
		
	}
	
	@Test
	/**
	 * Test per la Registrazione
	 * 
	 */
	@DisplayName("Test di registrazione")
	void testRegistrazione() {
		
		Registration.clearDB();
		ArrayList<String> dati= new ArrayList<String>();
		for(int i=0; i<11; i++) {
			dati.add("Utenteinfo");
		}
		assertEquals("Registazione completata con successo", Registration.doRegistration(dati) );
		assertEquals("Username gi� presente",  Registration.doRegistration(dati));
	}
	
	@Test
	/**
	 * Test per l'inserimento di una categoria.
	 */
	@DisplayName("Test categoria")
	void testCategoria() {
		
		CategoriaDB.clearDB();
		
		//categoria gi� esistente
		CategoriaDB.categorieDefault();
		assertEquals("categoria gia' presente",CategoriaDB.aggiungiCategoria("","Sport"));
		
		//successo aggiunta
		assertEquals("aggiunta finita con successo", CategoriaDB.aggiungiCategoria("Sport","Scarpe"));


		//caso aggiunta vuoto
		assertEquals("inserisci categoria",CategoriaDB.aggiungiCategoria("Sport", ""));

		//rinomina categoria
		assertEquals("Rinominata con successo",CategoriaDB.rinominaCategoria("Scarpe","Canoe"));

	}
	@Test
	/**
	 * Test per l'inserimento di una domanda.
	 */
	@DisplayName("Test domanda")
	void testDomanda() {

		Categoria Abbigliamento = new Categoria("Abbigliamento");
		Oggetto o = new Oggetto("marco","bhu","bhu", 1.0 , "5/11/2020", Abbigliamento);
		//AstaDB.inserimentoAsta("utenteinfo", "bhu", Abbigliamento.getNome(), 1.0, "5/11/2020", "bhu");
		assertEquals("Domanda inviata al venditore con successo.", MessaggiDB.aggiungiDomanda("LA MIA DOMANDA", "utenteinfo", o));
	}


	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}