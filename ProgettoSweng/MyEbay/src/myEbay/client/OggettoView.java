package myEbay.client;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;

import myEbay.shared.Domanda;
import myEbay.shared.Oggetto;
import myEbay.shared.Sessione;
import myEbay.shared.StatoAsta;

public class OggettoView {

	final VerticalPanel mainPanel;
	private Oggetto oggetto;
	private Domanda domandaSelezionata;

	public OggettoView(VerticalPanel vp, Oggetto oggetto) {
		this.oggetto = oggetto;
		this.mainPanel = vp;
	}

	@SuppressWarnings("unlikely-arg-type")
	public void onModuleLoad() {

		NavBarChoose menu = Sessione.getSessione().equals("") ? new NavBarChoose(mainPanel, 1) : new NavBarChoose(mainPanel, 2);
		menu.onModuleLoad();

		mainPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		HTML descrizione = new HTML("<h1>SCHEDA DEL PRODOTTO</h1>");
		mainPanel.add(descrizione);

		final Label labelNomeOggetto = new Label("Nome Oggetto:");
		final Label nomeOggetto = new Label(oggetto.getNome());
		final Label labelDescrizione = new Label("Descrizione Oggetto:");
		final Label descrizioneOggetto = new Label(oggetto.getDescrizione());
		final Label labelBaseAsta = new Label("Base d'Asta:");
		final Label baseAsta = new Label(String.valueOf(oggetto.getBaseAsta()));
		final Label labelScadenza = new Label("Scadenza Asta:");
		final Label scadenzaAsta = new Label(oggetto.getScadenza());
		final Label labelCategoria = new Label("Categoria:");
		final Label categoria = new Label(oggetto.getCategoria().getNome());
		final Label labelVenditore = new Label("Nome Venditore:");
		final Label venditore = new Label(oggetto.getVenditore());
		final Label labelOffertaCorrente = new Label("Maggiore Offerta Corrente:");
		final Label offertaCorrente = new Label(String.valueOf(oggetto.getOffertaMax()));
		final Label labelStato = new Label("Stato Asta:");
		final Label stato = new Label(oggetto.getStato());
		final Label labelVincitore = new Label("Vincitore Asta:");
		Label vincitore = new Label((stato.getText().equals(StatoAsta.ASTACONCLUSA)) ? (oggetto.getOfferta().getLast().getOfferente().getUsername()) : "");


		Grid grigliaOggetto = new Grid(9, 2);

		grigliaOggetto.setWidget(0, 0, labelNomeOggetto);
		grigliaOggetto.setWidget(0, 1, nomeOggetto);
		grigliaOggetto.setWidget(1, 0, labelDescrizione);
		grigliaOggetto.setWidget(1, 1, descrizioneOggetto);
		grigliaOggetto.setWidget(2, 0, labelBaseAsta);
		grigliaOggetto.setWidget(2, 1, baseAsta);
		grigliaOggetto.setWidget(3, 0, labelScadenza);
		grigliaOggetto.setWidget(3, 1, scadenzaAsta);
		grigliaOggetto.setWidget(4, 0, labelCategoria);
		grigliaOggetto.setWidget(4, 1, categoria);
		grigliaOggetto.setWidget(5, 0, labelVenditore);
		grigliaOggetto.setWidget(5, 1, venditore);
		grigliaOggetto.setWidget(6, 0, labelOffertaCorrente);
		grigliaOggetto.setWidget(6, 1, offertaCorrente);
		grigliaOggetto.setWidget(7, 0, labelStato);
		grigliaOggetto.setWidget(7, 1, stato);
		grigliaOggetto.setWidget(8, 0, labelVincitore);
		grigliaOggetto.setWidget(8, 1, vincitore);

		grigliaOggetto.addStyleName("MyGrid");
		mainPanel.add(grigliaOggetto);
		mainPanel.add(new HTML("</br>"));

		final Label titolo = new Label("INSERISCI OFFERTA:");
		final TextBox importo = new TextBox();
		Button btn = new Button("FAI OFFERTA");

		if(Sessione.getSessione().equals("")||Sessione.getSessione().equals(oggetto.getVenditore())) {
			btn.setEnabled(false);
		}

		mainPanel.add(titolo);
		mainPanel.add(importo);
		mainPanel.add(btn);
		btn.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				double d = Double.valueOf(importo.getText());
				GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
				greetingService.doOfferta(d, Sessione.getSessione(), oggetto, new AsyncCallback<String>() {

					@Override
					public void onFailure(Throwable caught) {
						HTML message= new HTML(caught.getMessage());
						mainPanel.add(message);	
					}

					@Override
					public void onSuccess(String result) {
						final DialogBox messaggio = new DialogBox();
						final String[] split = result.split("\\|");
						messaggio.setText(split[0]);
						messaggio.setPopupPosition(250, 250);
						VerticalPanel vPanel = new VerticalPanel();

						vPanel.add(new Button("Chiudi", new ClickHandler() {
							public void onClick(ClickEvent event) {

								if(split[1]!=null) {

									if(oggetto.getOfferta().size()<=0) {
										offertaCorrente.setText(split[1]);
									}
									else {

										mainPanel.clear();
										oggetto.getOfferta().getLast().setImporto(Double.valueOf(split[1])); 
										OggettoView scheda = new OggettoView(mainPanel, oggetto);
										scheda.onModuleLoad();
									}
								}
								messaggio.hide();
							}
						}));
						messaggio.setWidget(vPanel);		
						messaggio.show();
					}
				});	
			}
		});

		HTML title = new HTML("<h1>DOMANDE E RISPOSTE UTILI</h1>");
		mainPanel.add(title);

		ArrayList <Domanda> domandeERisposte = new ArrayList<Domanda>();
		for(int i=0; i<oggetto.getDomande().size(); i++) {
			domandeERisposte.add(oggetto.getDomande().get(i));
		}
		//crea una cellTable
		final CellTable<Domanda> table = new CellTable<Domanda>(500);
		table.setKeyboardSelectionPolicy (KeyboardSelectionPolicy.ENABLED);

		// Add a text column to show the text.
		TextColumn<Domanda> textColumn = new TextColumn<Domanda>() {
			@Override
			public String getValue(Domanda d) {
				return d.getTesto();
			}
		};
		table.addColumn(textColumn, "DOMANDE degli UTENTI");

		//Add a text column to show the username.
		TextColumn<Domanda> usernameColumn = new TextColumn<Domanda>() {
			@Override
			public String getValue(Domanda d) {
				return d.getMittente().getUsername();
			}
		};
		table.addColumn(usernameColumn, "Username");

		TextColumn<Domanda> rispostaColumn = new TextColumn<Domanda>() {
			@Override
			public String getValue(Domanda d) {
				if(d.getRisposta().getTesto()!=null) {
					return d.getRisposta().getTesto();
				}else {
					return "Risposta non inserita";
				}
			}
		};
		table.addColumn(rispostaColumn, "RISPOSTA del VENDITORE");

		//Add a text column to show the timestamp.
		TextColumn<Domanda> timeColumn = new TextColumn<Domanda>() {
			@Override
			public String getValue(Domanda d) {
				return d.getTimestamp().toString();
			}
		};
		table.addColumn(timeColumn, "dettagli");

		table.setWidth("90%", true);
		table.setColumnWidth(textColumn, 20.0 , Unit.PCT);
		table.setColumnWidth(usernameColumn, 20.0 , Unit.PCT);
		table.setColumnWidth(timeColumn, 20.0 , Unit.PCT);
		table.setColumnWidth(rispostaColumn, 20.0 , Unit.PCT);

		table.setRowCount(domandeERisposte.size(), true);
		// Push the data into the widget.
		table.setRowData(0, domandeERisposte);
		mainPanel.add(table);
		mainPanel.add(new HTML("<br>"));
		
		HorizontalPanel hp = new HorizontalPanel();
		final Button rispondi = new Button("RISPONDI");
		rispondi.setEnabled(false);
			
		final SingleSelectionModel<Domanda> selectionModel = new SingleSelectionModel<Domanda>();
		table.setSelectionModel(selectionModel);
		selectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
			public void onSelectionChange(SelectionChangeEvent event) {
				if(Sessione.getSessione().equals(oggetto.getVenditore())) {
					domandaSelezionata = selectionModel.getSelectedObject();
					rispondi.setEnabled(true);
				}
			}
		});

		rispondi.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				RispostaView sezioneR = new RispostaView(mainPanel, oggetto, domandaSelezionata);
				sezioneR.onModuleLoad();					
			}
		});


		Button info = new Button("CHIEDI INFORMAZIONI");

		if(Sessione.getSessione().equals("")|| oggetto.getStato().equals(StatoAsta.ASTACONCLUSA)||
				oggetto.getStato().equals(StatoAsta.ASTASCADUTA)) {
			info.setEnabled(false);
		}
		info.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				DomandaView sezioneD = new DomandaView(mainPanel, oggetto);
				sezioneD.onModuleLoad();

			}		
		});

		hp.add(info);
		hp.add(rispondi);

		mainPanel.add(hp);
		RootPanel.get().add(mainPanel);
	}
}
