package myEbay.client;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import myEbay.shared.Sessione;

public class Logout {

	VerticalPanel vp;
	public Logout(VerticalPanel vp) {
		this.vp = vp;
	}
	public void onModuleLoad() {
		// TODO Auto-generated method stub
		Sessione.setSessione("");
		final DialogBox messaggio = new DialogBox();
		messaggio.setText("ARRIVEDERCI! GRAZIE PER AVER USATO MYEBAY.");
		messaggio.setPopupPosition(250, 200);
		VerticalPanel vPanel = new VerticalPanel();

		vPanel.add(new Button("X", new ClickHandler() {
			public void onClick(ClickEvent event) {
				messaggio.hide();
				vp.clear();
				MyEbay homeutenteVis = new MyEbay();
				homeutenteVis.onModuleLoad();
				
			}
		}));
		
		messaggio.setWidget(vPanel);		
		messaggio.show();
		
	}

}
