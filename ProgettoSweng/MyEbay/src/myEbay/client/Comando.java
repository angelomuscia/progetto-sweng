package myEbay.client;

import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.ui.VerticalPanel;

public class Comando {
	
	private VerticalPanel verticalPanel;
	Command cmd= null;
	
	public Comando(VerticalPanel vpanel) {
		this.verticalPanel = vpanel;
		
	}
	
	//comando ritorno alla home
	Command cmdHomeVisitatore = new Command()	{
		public void execute()	{
			verticalPanel.clear();
			MyEbay homeutenteVis = new MyEbay();
			homeutenteVis.onModuleLoad();
		}
	};
	
	//comando ritorno alla home utente
	Command cmdHomeUtente = new Command()	{
		public void execute()	{
			verticalPanel.clear();
			HomeRegistrato homeutente = new HomeRegistrato(verticalPanel);
			homeutente.onModuleLoad();
		}
	};
			
	//comando ritorno alla home amministratore
	Command cmdHomeAdmin = new Command()	{
		public void execute()	{
			verticalPanel.clear();
			HomeAdm homeadm = new HomeAdm(verticalPanel);
			homeadm.onModuleLoad();
		}
	};
	//comando modifica categoria
	Command cmdModC = new Command()	{
		public void execute()	{
			verticalPanel.clear();
			ModificaCategoriaView modCat = new ModificaCategoriaView(verticalPanel);
			modCat.onModuleLoad();
		}
	};
	//comando lista oggetti
	Command cmdobj = new Command()	{
		public void execute()	{
			verticalPanel.clear();
			ListaOggettiView listaObj = new ListaOggettiView(verticalPanel);
			listaObj.onModuleLoad();
		}
	};
	//comando Logout
	Command cmdLogout = new Command()	{
		public void execute()	{
			verticalPanel.clear();
			Logout cmdLog = new Logout(verticalPanel);
			cmdLog.onModuleLoad();
		}
	};
	//comando login
	Command cmdLogin = new Command() {
		public void execute() {
			verticalPanel.clear();
			LoginView login = new LoginView(verticalPanel);
			login.onModuleLoad();
		}
	};
	//comando registrazione
	Command cmdRegistrazione = new Command() {
		public void execute() {
			verticalPanel.clear();
			RegistrationView  registrazione = new RegistrationView(verticalPanel);
			registrazione.onModuleLoad();
		}
	};
	//comando vendita oggetto
	Command cmdVendi = new Command()	{
		public void execute()	{
			verticalPanel.clear();
			VenditaOggettoView vendi = new VenditaOggettoView(verticalPanel);
			vendi.onModuleLoad();
		}
	};
	//comando visualizzare pagina profilo utente
	Command cmdProfilo = new Command() {
		public void execute() {
			verticalPanel.clear();
			VisualizzaProfiloView newInfo = new VisualizzaProfiloView(verticalPanel);
			newInfo.onModuleLoad();
		}
	};
	//comando nuova categoria
	Command cmdNewCat = new Command()	{
		public void execute()	{
			verticalPanel.clear();
			NuovaCategoriaView newC = new NuovaCategoriaView();
			newC.onModuleLoad();
		}
	};

}


