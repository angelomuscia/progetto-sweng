package myEbay.client;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.VerticalPanel;

public class MyDialog extends DialogBox {

	final DialogBox toast;
	VerticalPanel vP = new VerticalPanel();

	public MyDialog(String testo) {
		this.toast= new DialogBox(false, true);
		this.toast.setText(testo);
	}

	// DialogBox is a SimplePanel, so you have to set its widget property to
	// whatever you want its contents to be.
	public DialogBox showMessaggio() {
		final Button ok = new Button("Chiudi");
		ok.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				MyDialog.this.hide();

			}
		});
		ok.getElement().setAttribute("align", "center");

		vP.add(toast);
		vP.add(ok);

		this.toast.setWidget(vP);
		this.toast.center();
		this.toast.show();
		return toast;

	}
}

