package myEbay.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import myEbay.shared.Domanda;
import myEbay.shared.Oggetto;


public class RispostaView {

	VerticalPanel vp;
	Oggetto oggetto;
	Domanda domanda;

	public RispostaView (VerticalPanel vp, Oggetto oggetto, Domanda domanda) {
		this.oggetto = oggetto;
		this.vp = vp;	
		this.domanda = domanda;
	}

	public void onModuleLoad() {

		final DialogBox messaggio = new DialogBox();
		messaggio.setText("INSERISCI LA TUA RISPOSTA:");
		messaggio.setPopupPosition(550, 550);
		final TextBox campotesto = new TextBox();
		campotesto.setPixelSize(250, 250);
		final Button salva = new Button("SALVA");
		salva.setEnabled(false);

		VerticalPanel vPanel = new VerticalPanel();


		campotesto.addKeyUpHandler(new KeyUpHandler() {

			@Override
			public void onKeyUp(KeyUpEvent event) {
				// TODO Auto-generated method stub
				if (!(campotesto.getValue().isEmpty() || campotesto.getValue().trim().length()<10)) {
					salva.setEnabled(true);
				}
			}
		});

		vPanel.setSpacing(6);
		vPanel.add(campotesto);
		vPanel.setHorizontalAlignment(VerticalPanel.ALIGN_CENTER);
		vPanel.add(salva);	

		salva.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				
				final GreetingServiceAsync greetingservice = GWT.create(GreetingService.class);
				greetingservice.aggiungiRisposta(campotesto.getText(), oggetto.getVenditore(), domanda, oggetto, new AsyncCallback<Oggetto>() {

					@Override
					public void onFailure(Throwable caught) {
						HTML message= new HTML(caught.getMessage());
						vp.add(message);	

					}

					@Override
					public void onSuccess(Oggetto result) {

						final Oggetto obj = result;
						final DialogBox messaggio = new DialogBox();
						messaggio.setText("Risposta inserita con successo!");
						messaggio.setPopupPosition(500, 500);
						VerticalPanel vPanel = new VerticalPanel();

						vPanel.add(new Button("Chiudi", new ClickHandler() {
							public void onClick(ClickEvent event) {
								vp.clear();
								OggettoView scheda = new OggettoView(vp, obj);
								scheda.onModuleLoad();
								messaggio.hide();
							}

						}));
						messaggio.setWidget(vPanel);		
						messaggio.show();

					}
				});

				messaggio.hide();
			}
		});
		messaggio.setWidget(vPanel);		
		messaggio.show();
	}
}


