package myEbay.client;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import myEbay.shared.Categoria;

public class ListBoxCateg {

	public static void setList(final VerticalPanel vp, final ListBox listaCategorie) {
		GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
		greetingService.getCategorie( new AsyncCallback<ArrayList<Categoria>> () {

			@Override
			public void onFailure(Throwable caught) {
				HTML failure = new HTML(caught.getMessage());
				vp.add(failure);
			}

			@Override
			public void onSuccess(ArrayList<Categoria> result) {
				listaCategorie.addItem("--SELEZIONA CATEGORIA--","NULL");
				for(Categoria categoria : result) {
					listaCategorie.addItem(categoria.getNome(), categoria.getNome());
				}

			}


		});
	}
	public static void setList(final String catePadre, final VerticalPanel vp, final ListBox listaCategorie) {
		GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
		greetingService.getSottoCategorie(catePadre, new AsyncCallback<ArrayList<Categoria>> () {

			@Override
			public void onFailure(Throwable caught) {
				HTML failure = new HTML(caught.getMessage());
				vp.add(failure);
			}

			@Override
			public void onSuccess(ArrayList<Categoria> result) {
				listaCategorie.addItem(catePadre);
				listaCategorie.addItem("<--Indietro","NULL");

				if(result.size()>0){
					for(Categoria categoria : result) {
						listaCategorie.addItem(categoria.getNome() , categoria.getNome());
					}
				}
			}
		});
	}

}
