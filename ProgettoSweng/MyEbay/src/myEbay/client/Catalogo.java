package myEbay.client;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import myEbay.shared.Oggetto;
import myEbay.shared.Sessione;

public class Catalogo {

	VerticalPanel vp;

	public Catalogo(VerticalPanel vp) {
		this.vp = vp;
	}

	public void onModuleLoad(){


		final VerticalPanel tabPanel = new VerticalPanel();

		final GreetingServiceAsync greetingservice =GWT.create(GreetingService.class);
		greetingservice.getOggetti(new AsyncCallback<ArrayList<Oggetto>>() {

			@Override
			public void onFailure(Throwable caught) {
				HTML error = new HTML(caught.getMessage());
				vp.add(error);				
			}

			@Override
			public void onSuccess(final ArrayList<Oggetto> result) {

				final FlexTable listaOggetti = new FlexTable();

				for(int i=0; i<result.size(); i++) {
					SetTable(listaOggetti,i,result.get(i));					
				}				
				tabPanel.add(listaOggetti);
			}			
		});
		tabPanel.setStyleName("center");


		if(!Sessione.getSessione().equals("")) {
			HorizontalPanel hp1 = new HorizontalPanel();

			vp.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
			HTML descrizione = new HTML("<h3>CATALOGO</h3>");
			vp.add(descrizione);

			HTML categorie = new HTML("FILTRA CATALOGO:   ");
			hp1.add(categorie);
			final ListBox elencoCategorie = new ListBox();
			ListBoxCateg.setList(vp, elencoCategorie);

			elencoCategorie.setVisibleItemCount(1);

			//handler listbox
			elencoCategorie.addChangeHandler(new ChangeHandler() {

				@Override
				public void onChange(ChangeEvent event) {
					if( elencoCategorie.getSelectedValue().equals("NULL")) {
						elencoCategorie.clear();
						ListBoxCateg.setList(vp,  elencoCategorie);
					}else {
						String value= elencoCategorie.getSelectedItemText();
						elencoCategorie.clear();
						ListBoxCateg.setList(value, vp, elencoCategorie);	
					}
				}
			});

			hp1.add(elencoCategorie);
			Button btn = new Button("CERCA");
			hp1.add(btn);
			vp.add(hp1);
			btn.addClickHandler(new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) {

					final GreetingServiceAsync greetingservice = GWT.create(GreetingService.class);
					greetingservice.oggettiFiltrati(elencoCategorie.getSelectedItemText(), new AsyncCallback<ArrayList<Oggetto>>() {

						@Override
						public void onFailure(Throwable caught) {
							tabPanel.clear();
							HTML message = new HTML(caught.getMessage());
							tabPanel.add(message);

						}

						@Override
						public void onSuccess(ArrayList<Oggetto> result) {
							tabPanel.clear();
							final FlexTable listaOggetti = new FlexTable();

							for(int i=0; i<result.size(); i++) {
								SetTable(listaOggetti,i,result.get(i));					
							}				
							tabPanel.add(listaOggetti);
						}
					});
					tabPanel.setStyleName("center");							
				}
			});
		}
		vp.add(tabPanel);
	}


	public void SetTable(final FlexTable listaOggetti, final int index, final Oggetto result ) {
		listaOggetti.setText(index,0,result.getNome()+
				"  VENDUTO DA : "+result.getVenditore()+"  SCADENZA : "+result.getScadenza());

		if(!Sessione.getSessione().equals("admin")) {
			listaOggetti.setWidget(index, 1, new Button("Dettagli", new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) {
					vp.clear();
					final VerticalPanel newVP = new VerticalPanel();
					Oggetto oggetto = result;
					OggettoView scheda = new OggettoView(newVP,oggetto);
					scheda.onModuleLoad();
				}
			}));
		}
		else {
			listaOggetti.setWidget(index, 1, new Button("ELIMINA", new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) {

					final Oggetto obj = result;
					vp.clear();
					OggettoAdminView scelta = new OggettoAdminView(vp, obj);
					scelta.onModuleLoad();			
				}	
			}));
		}
	}
}

