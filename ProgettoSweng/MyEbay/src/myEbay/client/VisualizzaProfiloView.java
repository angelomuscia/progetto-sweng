package myEbay.client;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

import myEbay.shared.Offerta;
import myEbay.shared.Oggetto;
import myEbay.shared.Sessione;
import myEbay.shared.UtenteRegistrato;

public class VisualizzaProfiloView {

	private VerticalPanel mainPanel = new VerticalPanel();

	public VisualizzaProfiloView(VerticalPanel vp)	{
		this.mainPanel= vp;
	}

	public void onModuleLoad()	{

		NavBarChoose menuUtente = new NavBarChoose(mainPanel, 2);
		menuUtente.onModuleLoad();

		mainPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		HTML scheda1 = new HTML("<h3>DATI DI REGISTRAZIONE:</h3>");
		mainPanel.add(scheda1);

		String nomeUtente = Sessione.getSessione();

		final GreetingServiceAsync greetingservice = GWT.create(GreetingService.class);
		greetingservice.getUtente(nomeUtente, new AsyncCallback<UtenteRegistrato>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onSuccess(UtenteRegistrato result) {

				final Label labelUsername = new Label("Username:");
				final Label labelNome = new Label("Nome:");		
				final Label labelCognome = new Label("Cognome:");		
				final Label labelIndirizzo = new Label("Indirizzo:");		
				final Label labelNumero = new Label("Numero di Telefono:");		
				final Label labelMail = new Label("Email:");		
				final Label labelpsw = new Label("Password:");		
				final Label labelCF = new Label("Codice Fiscale:");		
				final Label labelSesso = new Label("Sesso:");		
				final Label labelLuogoN = new Label("Luogo di Nascita:");		
				final Label labelData = new Label("Data di Nascita:");
				final Label Username = new Label(result.getUsername());
				final Label Nome = new Label(result.getNome());
				final Label Cognome = new Label(result.getCognome());
				final Label Indirizzo = new Label(result.getIndirizzo());
				final Label Numero = new Label(result.getNumeroTelefono());
				final Label Email = new Label(result.getEmail());
				final Label Password = new Label(result.getPassword());
				final Label CF = new Label(result.getCodiceFiscale());
				final Label sesso = new Label(result.getSesso());
				final Label Luogo = new Label(result.getLuogoNascita());
				final Label Data = new Label(result.getDataNascita());

				Grid panelProfilo = new Grid(11,2);
				panelProfilo.setWidget(0, 0, labelUsername);
				panelProfilo.setWidget(0, 1, Username);
				panelProfilo.setWidget(1, 0, labelNome);
				panelProfilo.setWidget(1, 1, Nome);
				panelProfilo.setWidget(2, 0, labelCognome);
				panelProfilo.setWidget(2, 1, Cognome);
				panelProfilo.setWidget(3, 0, labelIndirizzo);
				panelProfilo.setWidget(3, 1, Indirizzo);
				panelProfilo.setWidget(4, 0, labelNumero);
				panelProfilo.setWidget(4, 1, Numero);
				panelProfilo.setWidget(5, 0, labelMail);
				panelProfilo.setWidget(5, 1, Email);
				panelProfilo.setWidget(6, 0, labelpsw);
				panelProfilo.setWidget(6, 1, Password);
				panelProfilo.setWidget(7, 0, labelCF);
				panelProfilo.setWidget(7, 1, CF);
				panelProfilo.setWidget(8, 0, labelLuogoN);
				panelProfilo.setWidget(8, 1, Luogo);
				panelProfilo.setWidget(9, 0, labelData);
				panelProfilo.setWidget(9, 1, Data);
				panelProfilo.setWidget(10, 0, labelSesso);
				panelProfilo.setWidget(10, 1, sesso);
				
				panelProfilo.addStyleName("MyGrid");
				
				mainPanel.add(panelProfilo);

			}

		});

		String utente = Sessione.getSessione();
		final GreetingServiceAsync greet =GWT.create(GreetingService.class);
		greet.myObjs(utente, new AsyncCallback<ArrayList<Oggetto>>() {

			@Override
			public void onFailure(Throwable caught) {
				HTML message = new HTML(caught.getMessage());
				mainPanel.add(message);
			}

			@Override
			public void onSuccess(ArrayList<Oggetto> result) {
				HTML titolo = new HTML("<h1>I TUOI OGGETTI IN VENDITA</h1>");
				mainPanel.add(titolo);
				final FlexTable listaOggetti = new FlexTable();

				for(int i=0; i<result.size(); i++) {
					SetTable(listaOggetti,i,result.get(i));					
				}				
				mainPanel.add(listaOggetti);
			}			

		});
		
		final GreetingServiceAsync greeting =GWT.create(GreetingService.class);
		greeting.mieOfferte(utente, new AsyncCallback<ArrayList<Oggetto>>() {

			@Override
			public void onFailure(Throwable caught) {
				HTML message = new HTML(caught.getMessage());
				mainPanel.add(message);
			}

			@Override
			public void onSuccess(ArrayList<Oggetto> result) {
				HTML titolo = new HTML("<h1>LE TUE OFFERTE</h1>");
				mainPanel.add(titolo);
				FlexTable listaOfferte = new FlexTable();

				for(int i=0; i<result.size(); i++) {
					for(Offerta offerta: result.get(i).getOfferta()) {
						if(offerta.getOfferente().getUsername().equals(Sessione.getSessione()))
							listaOfferte.setText(i,0,"- OGGETTO: "+result.get(i).getNome()+"  IMPORTO: "+ offerta.getImporto()+"  STATO: "+offerta.getStato());
					}
					
				}				
				mainPanel.add(listaOfferte);
			}			

		});

		
		RootPanel.get().add(mainPanel);
	}

	public void SetTable(final FlexTable listaOggetti, final int index, final Oggetto result ){
		listaOggetti.setText(index,0,"NOME:  "+result.getNome()+"  SCADENZA:   "+result.getScadenza()+"  STATO:  "+result.getStato());

		listaOggetti.setWidget(index, 1, new Button("Dettagli", new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				mainPanel.clear();
				final VerticalPanel newVP = new VerticalPanel();
				Oggetto oggetto = result;
				OggettoView scheda = new OggettoView(newVP,oggetto);
				scheda.onModuleLoad();
			}
		}));
	}

}
//FlexTable aggiungere offerte 


	
