package myEbay.client;

import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

public class ListaOggettiView {
	
	 private VerticalPanel mainPanel = new VerticalPanel();
    public ListaOggettiView(VerticalPanel vp) {
    	this.mainPanel = vp;

    }

    public void onModuleLoad() {
        mainPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
        HTML descrizione = new HTML("<h2>Lista Oggetti:</h2>");
        mainPanel.add(descrizione);

        final Label labelNomeOggetto = new Label("Nome Oggetto:");
        final TextBox nomeOggetto = new TextBox();
        final Label labelScadenza = new Label("Scadenza Asta:");
        final TextBox scadenzaAsta = new TextBox();
        final Label labelVenditore = new Label("Nome Venditore:");
        final TextBox venditore = new TextBox();

        Grid grigliaOggetto = new Grid(3, 2);
        FlexTable tableOggetti = new FlexTable();

        grigliaOggetto.setWidget(0, 0, labelNomeOggetto);
        grigliaOggetto.setWidget(0, 1, nomeOggetto);
        grigliaOggetto.setWidget(2, 0, labelScadenza);
        grigliaOggetto.setWidget(2, 1, scadenzaAsta);
        grigliaOggetto.setWidget(3, 0, labelVenditore);
        grigliaOggetto.setWidget(3, 1, venditore);

        tableOggetti.add(grigliaOggetto);

        mainPanel.add(tableOggetti);

        mainPanel.setStyleName("center");

    }
}
