package myEbay.client;


import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;



public class MyEbay implements EntryPoint {

		final VerticalPanel main = new VerticalPanel();
		Comando cmd= new Comando(main);
		

	@Override
	public void onModuleLoad() {
		
		NavBarChoose menu = new NavBarChoose(main, 1);
		menu.onModuleLoad();
		Catalogo catalogo = new Catalogo(main);
		catalogo.onModuleLoad();
		
		main.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		RootPanel.get().add(main);
		

	}
}


