package myEbay.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import myEbay.shared.Sessione;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;


/**
 *
 * Pagina che permette di fare il login nel sito di aste.
 *
 */
public class LoginView {
	
	private VerticalPanel mainPanel;
	private String user;
	private Boolean correctUser;
	
	public LoginView(VerticalPanel vP1) {
		this.mainPanel = vP1;
		correctUser = false;
	}
	
	public LoginView(VerticalPanel vP1,String user) {
		this.mainPanel = vP1;
		correctUser = true;
		this.user = user;
	}
	
	public void onModuleLoad() {
		
		//metodo per inizializzare il db 
		/*GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
		greetingService.inizializzazione(new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(Void result) {
				// TODO Auto-generated method stub
				
			}
		});*/
		
		NavBarChoose menu = new NavBarChoose(mainPanel,1);
		menu.onModuleLoad();
		
		mainPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		HTML descrizione = new HTML("<h3>INSERISCI LE TUE CREDENZIALI:</h3>");
		mainPanel.add(descrizione);
		
		final Label label1 = new Label("Username:");
		final TextBox usernameTxt = new TextBox();
		final Label label2 = new Label("Password:");
		final PasswordTextBox passwordTxt = new PasswordTextBox();
		
		usernameTxt.setText(correctUser ? user : "");
		
		final Grid grid = new Grid(2,2);
		grid.setWidget(0, 0, label1);
		grid.setWidget(0, 1, usernameTxt);
		grid.setWidget(1, 0, label2);
		grid.setWidget(1, 1, passwordTxt);
		
		final Button submit = new Button("ACCEDI");
		
		submit.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				
				GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
				greetingService.doLogin(usernameTxt.getText(), passwordTxt.getText(), new AsyncCallback<Integer> () {
					
					@Override
					public void onFailure(Throwable caught) {
						HTML message= new HTML(caught.getMessage());
						mainPanel.add(message);
					}

					@Override
					public void onSuccess(Integer result) {
						
						mainPanel.clear();
						LoginView tentativo;
						switch(result) {
						case -1:
							tentativo = new LoginView(mainPanel, usernameTxt.getText());
							tentativo.onModuleLoad();
							HTML error1 = new HTML("<h3>Password errata</h3>");
							mainPanel.add(error1);
							break;
						case 0: 
							tentativo = new LoginView(mainPanel);
							tentativo.onModuleLoad();
							HTML error = new HTML("<h3>Utente non registrato</h3>");
							mainPanel.add(error);
							break;
						case 1:
							Sessione.setSessione(usernameTxt.getText());
							HomeAdm homeAdmin = new HomeAdm(mainPanel);
							homeAdmin.onModuleLoad();
							break;
						case 2:
							Sessione.setSessione(usernameTxt.getText());
							HomeRegistrato homeUtente = new HomeRegistrato(mainPanel);
							homeUtente.onModuleLoad();
							break;
					
						}
					}
				});	
			}
		});
		
		mainPanel.add(grid);
		mainPanel.add(new HTML("</br>"));
		mainPanel.add(submit);
		
		RootPanel.get().add(mainPanel);
			
	}

}
