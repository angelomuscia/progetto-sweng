package myEbay.client;


import java.util.ArrayList;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import myEbay.shared.Categoria;
import myEbay.shared.Domanda;
import myEbay.shared.Offerta;
import myEbay.shared.Oggetto;
import myEbay.shared.UtenteRegistrato;

/**
 * The client-side stub for the RPC service.
 */
@RemoteServiceRelativePath("greet")
public interface GreetingService extends RemoteService {

	void inizializzazione() throws IllegalArgumentException;

	int doLogin(String username, String password) throws IllegalArgumentException;

	String doRegistration(ArrayList<String> infoutente) throws IllegalArgumentException;

	ArrayList<Categoria> getCategorie() throws IllegalArgumentException;

	ArrayList<Categoria> getSottoCategorie(String padrecategorie)  throws IllegalArgumentException;

	String inserimentoAsta(String venditore, String nome, String categoria, double baseAsta, String scadenza,
			String descrizione) throws IllegalArgumentException;

	String rinominaCategoria(String nuovo, String vecchio) throws IllegalArgumentException;

	String aggiungiCategoria(String padre, String nome ) throws IllegalArgumentException;

	UtenteRegistrato getUtente(String nomeUtente) throws IllegalArgumentException;

	ArrayList<Oggetto> getOggetti() throws IllegalArgumentException;

	ArrayList<Oggetto> oggettiFiltrati(String categoria) throws IllegalArgumentException;

	ArrayList<Oggetto> myObjs(String utente) throws IllegalArgumentException;

	String doOfferta(double importo, String offerente, Oggetto oggetto) throws IllegalArgumentException;

	ArrayList<Oggetto> mieOfferte (String utente) throws IllegalArgumentException;

	Oggetto aggiungiDomanda(String testo, String mittente, Oggetto oggettoCommentato);

	Oggetto aggiungiRisposta(String testo, String venditore, Domanda domanda, Oggetto oggettoCommentato);

	String eliminaOggetto(Oggetto oggetto) throws IllegalArgumentException;

	Oggetto eliminaDomanda(Oggetto oggetto, Domanda domanda) throws IllegalArgumentException;

	Oggetto eliminaRisposta(Domanda domandaDiRiferimento, Oggetto oggetto) throws IllegalArgumentException;

	Oggetto eliminaOfferta(Offerta offertaDaRimuovere, Oggetto oggetto ) throws IllegalArgumentException;
}
