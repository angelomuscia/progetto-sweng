package myEbay.client;


import java.util.Date;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.datepicker.client.DatePicker;

import myEbay.shared.Sessione;

public class VenditaOggettoView {

	private VerticalPanel mainPanel;
	Comando cmd = new Comando(mainPanel);

	public VenditaOggettoView(VerticalPanel vp){
		this.mainPanel = vp;		
	}

	public void onModuleLoad()	{

		NavBarChoose menuUtente = new NavBarChoose(mainPanel, 2);
		menuUtente.onModuleLoad();

		mainPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		HTML des = new HTML("<h2>Compila la scheda dell' oggetto da vendere:</h2>");
		mainPanel.add(des);

		final Label labelNomeOggetto = new Label("Nome Oggetto:");
		final TextBox nomeOggetto = new TextBox();
		final Label labelPrezzo = new Label("Prezzo vendita:");
		final TextBox prezzo = new TextBox();
		final Label labelDataScadenza = new Label("Data Scadenza:");
		// Create a date picker
		DatePicker datePicker = new DatePicker();
		final Label text = new Label();

		// Set the value in the text box when the user selects a date
		datePicker.addValueChangeHandler(new ValueChangeHandler<Date>() {
			public void onValueChange(ValueChangeEvent<Date> event) {
				Date date = event.getValue();
				String dateString = DateTimeFormat.getFormat("dd/MM/yyyy").format(date);
				text.setText(dateString);
			}
		});

		// Set the default value
		datePicker.setValue(new Date(), true);

		final Label labelDescrizione = new Label("Descrizione:");
		final TextBox descrizione = new TextBox();
		final Label labelCategoria = new Label("Categoria:");
		final ListBox listaCategorie = new ListBox();
		ListBoxCateg.setList(mainPanel, listaCategorie);
		listaCategorie.setVisibleItemCount(1);

		//handler listbox
		listaCategorie.addChangeHandler(new ChangeHandler() {

			@Override
			public void onChange(ChangeEvent event) {
				if(listaCategorie.getSelectedValue().equals("NULL")) {
					listaCategorie.clear();
					ListBoxCateg.setList(mainPanel, listaCategorie);
				}else {
					String value= listaCategorie.getSelectedItemText();
					listaCategorie.clear();
					ListBoxCateg.setList(value, mainPanel, listaCategorie);	
				}
			}
		}); 

		Grid griglia = new Grid(6,2);
		griglia.setWidget(0, 0, labelNomeOggetto);
		griglia.setWidget(0, 1, nomeOggetto);
		griglia.setWidget(1, 0, labelPrezzo);
		griglia.setWidget(1, 1, prezzo);
		griglia.setWidget(2, 0, labelDataScadenza);
		griglia.setWidget(2, 1, datePicker);
		griglia.setWidget(3, 0, new Label());
		griglia.setWidget(3, 1, text);
		griglia.setWidget(4, 0, labelCategoria);
		griglia.setWidget(4, 1, listaCategorie);
		griglia.setWidget(5, 0, labelDescrizione);
		griglia.setWidget(5, 1, descrizione);

		final Button submit = new Button("VENDI");
		submit.getElement().setAttribute("align", "center");
		submit.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				String venditore = Sessione.getSessione();
				final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
				greetingService.inserimentoAsta(venditore, nomeOggetto.getText(), listaCategorie.getSelectedItemText(),
						Double.valueOf(prezzo.getText()), text.getText(), descrizione.getText(), new AsyncCallback<String>() {


					@Override
					public void onFailure(Throwable caught) {
						mainPanel.add(new HTML(caught.getMessage()));
					}

					@Override
					public void onSuccess(String result) {
						if(result.equals("Oggetto inserito con successo")) {						

							final DialogBox messaggio = new DialogBox();
							messaggio.setText(result);
							messaggio.setPopupPosition(250, 200);
							VerticalPanel vPanel = new VerticalPanel();

							vPanel.add(new Button("Chiudi", new ClickHandler() {
								public void onClick(ClickEvent event) {
									messaggio.hide();
									mainPanel.clear();
									HomeRegistrato homeutente = new HomeRegistrato(mainPanel);
									homeutente.onModuleLoad();
								}
							}));
							messaggio.setWidget(vPanel);		
							messaggio.show();
						}
						else {
							mainPanel.add(new HTML(result));
						}
					}


				});
			}			
		});

		mainPanel.add(griglia);
		mainPanel.add(submit);

		RootPanel.get().add(mainPanel);

	}

}
