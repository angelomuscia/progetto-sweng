package myEbay.client;


import java.util.ArrayList;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import myEbay.shared.Categoria;
import myEbay.shared.Domanda;
import myEbay.shared.Offerta;
import myEbay.shared.Oggetto;
import myEbay.shared.UtenteRegistrato;

/**
 * The async counterpart of <code>GreetingService</code>.
 */
@RemoteServiceRelativePath("greet")
public interface GreetingServiceAsync {

	void inizializzazione(AsyncCallback<Void> callback) throws IllegalArgumentException;

	void doLogin(String username, String password, AsyncCallback<Integer> callback) throws IllegalArgumentException;

	void doRegistration(ArrayList<String> infoutente, AsyncCallback<String> callback) throws IllegalArgumentException;

	void inserimentoAsta(String venditore, String nome, String categoria,
			double baseAsta, String scadenza, String descrizione,
			AsyncCallback<String> callback ) throws IllegalArgumentException;

	void getSottoCategorie(String padrecategorie, AsyncCallback<ArrayList<Categoria>> callback) throws IllegalArgumentException;

	void getCategorie(AsyncCallback<ArrayList<Categoria>> callback) throws IllegalArgumentException;

	void rinominaCategoria(String nuovo, String vecchio, AsyncCallback<String> callback) throws IllegalArgumentException;

	void aggiungiCategoria(String padre, String nome, AsyncCallback<String> callback) throws IllegalArgumentException;

	void getUtente(String nomeUtente, AsyncCallback<UtenteRegistrato> callback) throws IllegalArgumentException;

	void getOggetti(AsyncCallback<ArrayList<Oggetto>> callback) throws IllegalArgumentException;

	void oggettiFiltrati(String categoria, AsyncCallback<ArrayList<Oggetto>> callback) throws IllegalArgumentException;

	void myObjs(String utente, AsyncCallback<ArrayList<Oggetto>> callback) throws IllegalArgumentException;

	void doOfferta(double importo, String offerente, Oggetto oggetto, AsyncCallback<String> callback) throws IllegalArgumentException;

	void mieOfferte(String utente, AsyncCallback<ArrayList<Oggetto>> callback) throws IllegalArgumentException;

	void aggiungiDomanda(String testo, String mittente, Oggetto oggettoCommentato, AsyncCallback<Oggetto> callback) throws IllegalArgumentException;

	void aggiungiRisposta(String testo, String venditore, Domanda domanda, Oggetto oggettoCommentato,
			AsyncCallback<Oggetto> callback) throws IllegalArgumentException;

	void eliminaOggetto(Oggetto oggetto, AsyncCallback<String> callback)  throws IllegalArgumentException;

	void eliminaDomanda(Oggetto oggetto, Domanda domanda, AsyncCallback<Oggetto> callback) throws IllegalArgumentException;

	void eliminaRisposta(Domanda domandaDiRiferimento, Oggetto oggetto, AsyncCallback<Oggetto> callback) throws IllegalArgumentException;

	void eliminaOfferta(Offerta offertaDaRimuovere, Oggetto oggetto, AsyncCallback<Oggetto> callback)throws IllegalArgumentException;
}
