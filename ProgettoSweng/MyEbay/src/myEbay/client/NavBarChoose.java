package myEbay.client;


import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.MenuBar;
import com.google.gwt.user.client.ui.VerticalPanel;

public class NavBarChoose {

	VerticalPanel vp;
	int tipoProfilo = 0;

	public NavBarChoose(VerticalPanel vp, int tipoProfilo) {
		this.vp = vp;
		this.tipoProfilo = tipoProfilo;

	}
	public void onModuleLoad() {
		
		vp.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		Comando cmd = new Comando(vp);
		MenuBar menu = new MenuBar();		
	
		if(tipoProfilo==3) {
			// navbar admin
			menu.addItem("HOME", cmd.cmdHomeAdmin); // href alla home
			menu.addSeparator();
			menu.addItem("MODIFICA CATEGORIA", cmd.cmdModC); // href alla pagina di modifica della categoria
			menu.addSeparator();
			menu.addItem("NUOVA CATEGORIA", cmd.cmdNewCat); // href alla pagina di modifica della categoria
			menu.addSeparator();
			menu.addItem("LOGOUT", cmd.cmdLogout); // href logout
		}
		if(tipoProfilo==2) {
			
			//navbar utente registarto
			
			menu.addItem("HOME", cmd.cmdHomeUtente); // href alla home
			menu.addSeparator();
			menu.addItem("VENDI", cmd.cmdVendi); // href alla pagina di vendita
			menu.addSeparator();
			menu.addItem("PROFILO", cmd.cmdProfilo);
			menu.addSeparator();
			menu.addItem("LOGOUT", cmd.cmdLogout); // href logout	

		}
		if(tipoProfilo==1) {
			
			//navbar utente non registarto
			menu.addItem("HOME", cmd.cmdHomeVisitatore); // href alla home
			menu.addSeparator();
			menu.addItem("LOGIN", cmd.cmdLogin); // href al Login
			menu.addSeparator();
			menu.addItem("REGISTRATI", cmd.cmdRegistrazione);// href alla registrazione
			

		}
		
		vp.add(menu);
		vp.setWidth("100%");
	}
}
