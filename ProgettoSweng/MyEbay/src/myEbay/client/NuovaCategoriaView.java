package myEbay.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

public class NuovaCategoriaView {

    public NuovaCategoriaView() {

    }

    private VerticalPanel mainPanel = new VerticalPanel();

    public void onModuleLoad() {
    	
    	NavBarChoose menuAmministratore = new NavBarChoose(mainPanel, 3);
		menuAmministratore.onModuleLoad();
		
		mainPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		HTML descrizione = new HTML("<h3>Nuova Categoria:</h3>");
		mainPanel.add(descrizione);

		Label labelNomeCategoria = new Label("Categoria di Appartenenza:");
		final ListBox elencoCategorie = new ListBox();
		elencoCategorie.addItem("","NULL");
		ListBoxCateg.setList(mainPanel, elencoCategorie);

		elencoCategorie.setVisibleItemCount(1);

		//handler listbox
		elencoCategorie.addChangeHandler(new ChangeHandler() {

			@Override
			public void onChange(ChangeEvent event) {
				if( elencoCategorie.getSelectedValue().equals("NULL")) {
					elencoCategorie.clear();
					ListBoxCateg.setList(mainPanel,  elencoCategorie);
				}else {
					String value= elencoCategorie.getSelectedItemText();
					elencoCategorie.clear();
					ListBoxCateg.setList(value, mainPanel, elencoCategorie);	
				}
			}
		}); 
		
		Label labelNuovaCategoria = new Label("Nome della nuova Categoria:");
		final TextBox nuovaCategoria = new TextBox();

		Grid griglia = new Grid(2, 2);
		griglia.setWidget(0, 0, labelNomeCategoria);
		griglia.setWidget(0, 1, elencoCategorie);
		griglia.setWidget(1, 0, labelNuovaCategoria);
		griglia.setWidget(1, 1, nuovaCategoria);

        final Button conferma = new Button("CREA CATEGORIA");
        conferma.getElement().setAttribute("align", "center");
        
        conferma.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				// TODO Auto-generated method stub
				final GreetingServiceAsync greetingservice = GWT.create(GreetingService.class);
				greetingservice.aggiungiCategoria(elencoCategorie.getSelectedItemText(), 
						nuovaCategoria.getText(), new AsyncCallback<String>() {

							@Override
							public void onFailure(Throwable caught) {
								// TODO Auto-generated method stub
								final DialogBox messaggio = new DialogBox();
								messaggio.setText(caught.getMessage());
								messaggio.setPopupPosition(250, 200);
								VerticalPanel vPanel = new VerticalPanel();

								vPanel.add(new Button("Chiudi", new ClickHandler() {
									public void onClick(ClickEvent event) {
										messaggio.hide();
									}
								}));
								messaggio.setWidget(vPanel);		
								messaggio.show();
							}

							@Override
							public void onSuccess(String result) {
								// TODO Auto-generated method stub
								if(result.equals("aggiunta finita con successo")) {
									final DialogBox messaggio = new DialogBox();
									messaggio.setText(result);
									messaggio.setPopupPosition(250, 200);
									final VerticalPanel vPanel = new VerticalPanel();

									vPanel.add(new Button("Chiudi", new ClickHandler() {
										public void onClick(ClickEvent event) {
											messaggio.hide();
											mainPanel.clear();
											HomeAdm homeadmin = new HomeAdm(mainPanel);
											homeadmin.onModuleLoad();
										}
									}));
									messaggio.setWidget(vPanel);		
									messaggio.show();
								}
								else {
									mainPanel.add(new HTML(result));
								}									
							}				
				});				
			}      	
        });
        
        mainPanel.add(griglia);
        mainPanel.add(new HTML("</br>"));
        mainPanel.add(conferma);
        mainPanel.setStyleName("center");
        RootPanel.get().add(mainPanel);
  
    }
}
