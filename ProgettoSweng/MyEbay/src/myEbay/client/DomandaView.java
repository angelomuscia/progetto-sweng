package myEbay.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import myEbay.shared.Oggetto;
import myEbay.shared.Sessione;

public class DomandaView {

	VerticalPanel vp;
	Oggetto oggetto;

	public DomandaView(VerticalPanel vp, Oggetto oggetto) {
		this.oggetto = oggetto;
		this.vp = vp;	
	}

	public void onModuleLoad() {

		final DialogBox messaggio = new DialogBox();
		messaggio.setText("INSERISCI LA TUA DOMANDA:");
		messaggio.setPopupPosition(550, 550);
		final TextBox campotesto = new TextBox();
		campotesto.setPixelSize(250, 250);
		final Button invia = new Button("INVIA");
		invia.setEnabled(false);

		VerticalPanel vPanel = new VerticalPanel();


		campotesto.addKeyUpHandler(new KeyUpHandler() {

			@Override
			public void onKeyUp(KeyUpEvent event) {
				// TODO Auto-generated method stub
				if (!(campotesto.getValue().isEmpty() || campotesto.getValue().trim().length()<10)) {
					invia.setEnabled(true);
				}
			}
		});

		vPanel.setSpacing(6);
		vPanel.add(campotesto);
		vPanel.setHorizontalAlignment(VerticalPanel.ALIGN_CENTER);
		vPanel.add(invia);	

		invia.addClickHandler( new ClickHandler() { 

			public void onClick(ClickEvent event) {

				final GreetingServiceAsync greetingservice = GWT.create(GreetingService.class);
				greetingservice.aggiungiDomanda(campotesto.getText(), Sessione.getSessione(), oggetto, new AsyncCallback<Oggetto>() {

					@Override
					public void onFailure(Throwable caught) {
						HTML message= new HTML(caught.getMessage());
						vp.add(message);	

					}

					@Override
					public void onSuccess(Oggetto result) {

						final Oggetto obj = result;
						final DialogBox messaggio = new DialogBox();
						messaggio.setText("Domanda inviata al venditore con successo!");
						messaggio.setPopupPosition(250, 200);
						VerticalPanel vPanel = new VerticalPanel();

						vPanel.add(new Button("Chiudi", new ClickHandler() {
							public void onClick(ClickEvent event) {
								vp.clear();
								OggettoView scheda = new OggettoView(vp, obj);
								scheda.onModuleLoad();
								messaggio.hide();
							}

						}));
						messaggio.setWidget(vPanel);		
						messaggio.show();
					}
				});

				messaggio.hide();
			}
		});
		messaggio.setWidget(vPanel);		
		messaggio.show();
	}
}
