package myEbay.client;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

public class RegistrationView {
	
	private VerticalPanel mainPanel;
	
	public RegistrationView(VerticalPanel v)	{
		this.mainPanel= v;
		
	}
	
	public void onModuleLoad() {
		
		NavBarChoose menu = new NavBarChoose(mainPanel,1);
		menu.onModuleLoad();
		
		mainPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		HTML descrizione = new HTML("<h3>FORM DI REGISTRAZIONE</h3>");
		
		mainPanel.add(descrizione);
		
		final Label labelUsername = new Label("Username*:");
		final TextBox usernameTxt = new TextBox();
		final Label labelNome = new Label("Nome*:");
		final TextBox nome = new TextBox();
		final Label labelCognome = new Label("Cognome*:");
		final TextBox cognome = new TextBox();
		final Label labelIndirizzo = new Label("Indirizzo*:");
		final TextBox indirizzo = new TextBox();
		final Label labelNumero = new Label("Numero di Telefono*:");
		final TextBox numero = new TextBox();
		final Label labelMail = new Label("Email*:");
		final TextBox emailTxt = new TextBox();
		final Label labelpsw = new Label("Password*:");
		final PasswordTextBox passwordTxt = new PasswordTextBox();
		final Label labelCF = new Label("Codice Fiscale*:");
		final TextBox cfBox = new TextBox();
		final Label s = new Label("Sesso:");
		final ListBox sesso = new ListBox();
		sesso.addItem("M");
		sesso.addItem("F");
		sesso.addItem("Altro");
		final Label labelLuogoN = new Label("Luogo di Nascita:");
		final TextBox luogoN = new TextBox();
		final Label labelData = new Label("Data di Nascita:");
		final TextBox dataN = new TextBox();
	    
	
		Grid panelRegistrazione = new Grid (11,2);
		
		panelRegistrazione.setWidget(0, 0, labelUsername);
		panelRegistrazione.setWidget(0, 1, usernameTxt);
		panelRegistrazione.setWidget(1, 0, labelNome);
		panelRegistrazione.setWidget(1, 1, nome);
		panelRegistrazione.setWidget(2, 0, labelCognome);
		panelRegistrazione.setWidget(2, 1, cognome);
		panelRegistrazione.setWidget(3, 0, labelIndirizzo);
		panelRegistrazione.setWidget(3, 1, indirizzo);
		panelRegistrazione.setWidget(4, 0, labelNumero);
		panelRegistrazione.setWidget(4, 1, numero);
		panelRegistrazione.setWidget(5, 0, labelMail);
		panelRegistrazione.setWidget(5, 1, emailTxt);
		panelRegistrazione.setWidget(6, 0, labelpsw);
		panelRegistrazione.setWidget(6, 1, passwordTxt);
		panelRegistrazione.setWidget(7, 0, labelCF);
		panelRegistrazione.setWidget(7, 1, cfBox);
		panelRegistrazione.setWidget(8, 0, labelLuogoN);
		panelRegistrazione.setWidget(8, 1, luogoN);
		panelRegistrazione.setWidget(9, 0, labelData);
		panelRegistrazione.setWidget(9, 1, dataN);
		panelRegistrazione.setWidget(10, 0, s);
		panelRegistrazione.setWidget(10, 1, sesso);
		
		final Button submit = new Button("REGISTRATI");
		submit.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
						
				ArrayList<String> arraydati =new ArrayList<>();
				arraydati.add(usernameTxt.getText());
				arraydati.add(nome.getText());
				arraydati.add(cognome.getText());
				arraydati.add(indirizzo.getText());
				arraydati.add(numero.getText());
				arraydati.add(emailTxt.getText());
				arraydati.add(passwordTxt.getText());
				arraydati.add(cfBox.getText());
				arraydati.add(sesso.getSelectedValue());
				arraydati.add(luogoN.getText());
				arraydati.add(dataN.getText());	
				
				final GreetingServiceAsync greetingservice = GWT.create(GreetingService.class);
				greetingservice.doRegistration(arraydati, new AsyncCallback<String>() {

					@Override
					public void onFailure(Throwable caught) {
						
							Window.alert(""+caught.getMessage()+"");		
						
						
					}

					@Override
					public void onSuccess(String result) {			
						
						if(result.equalsIgnoreCase("Registazione completata con successo")){

							mainPanel.clear();
							HomeRegistrato home = new HomeRegistrato(mainPanel,false);
							home.onModuleLoad();
							
						}else {
							final DialogBox messaggio = new DialogBox();
							messaggio.setText(result);
							messaggio.setPopupPosition(250, 200);
							VerticalPanel vPanel = new VerticalPanel();
						    
						    vPanel.add(new Button("Chiudi", new ClickHandler() {
						    public void onClick(ClickEvent event) {
						            messaggio.hide();
						        }
						    }));
						    messaggio.setWidget(vPanel);		
							messaggio.show();
						
						}
						
					}
					
				});
				
			}//end onClick()
			
		});
		
		Label avviso = new Label("* campi obligatori.");
		
		mainPanel.add(panelRegistrazione);
		mainPanel.add(new HTML("</br>"));
		
		mainPanel.add(avviso);
		mainPanel.add(new HTML("</br>"));
		
		mainPanel.add(submit);
		
		RootPanel.get().add(mainPanel);
		
		
	
	}

}
