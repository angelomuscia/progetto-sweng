package myEbay.client;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

public class HomeRegistrato {

	private VerticalPanel mainPanel;
	Comando cmd = new Comando(mainPanel);
	private Boolean log;

	public HomeRegistrato(VerticalPanel verticalPanel) {
		this.mainPanel = verticalPanel;
		this.log = true;
	}
	public HomeRegistrato(VerticalPanel verticalPanel, Boolean type) {
		this.mainPanel = verticalPanel;
		this.log = false;
	}


	public void onModuleLoad()	{
		
		if(!log) {
			
			final DialogBox messaggio = new DialogBox();
			messaggio.setText("Registrazione completata con successo");
			messaggio.setPopupPosition(250, 200);
			VerticalPanel vPanel = new VerticalPanel();
		    
		    vPanel.add(new Button("Chiudi", new ClickHandler() {
		    public void onClick(ClickEvent event) {
		            mainPanel.clear();
		            LoginView login = new LoginView(mainPanel);
					login.onModuleLoad();
					messaggio.hide();
		        }
		    }));
		    messaggio.setWidget(vPanel);		
			messaggio.show();
		}
		
		NavBarChoose menuUtente = new NavBarChoose(mainPanel, 2);
		menuUtente.onModuleLoad();
		
		Catalogo catalogo = new Catalogo(mainPanel);
		catalogo.onModuleLoad();
		
		RootPanel.get().add(mainPanel);
		
	}
}
