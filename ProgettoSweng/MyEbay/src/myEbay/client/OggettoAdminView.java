package myEbay.client;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;

import myEbay.shared.Domanda;
import myEbay.shared.Offerta;
import myEbay.shared.Oggetto;


public class OggettoAdminView {

	final VerticalPanel vp;
	private Oggetto oggetto;
	private Domanda domandaSelezionata;
	private Offerta offertaSelezionata;

	public OggettoAdminView(VerticalPanel vp, Oggetto oggetto) {
		this.oggetto = oggetto;
		this.vp = vp;
	}

	public void onModuleLoad() {

		//menu Sessione Admin
		NavBarChoose menu = new NavBarChoose(vp, 3);
		menu.onModuleLoad();
		
		HorizontalPanel hp = new HorizontalPanel();
		
		//handler eliminazione oggetto 
		Button btn1 = new Button("ELIMINA OGGETTO");
		btn1.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				
				GreetingServiceAsync greeting = GWT.create(GreetingService.class);
				greeting.eliminaOggetto(oggetto, new AsyncCallback<String>() {

					@Override
					public void onFailure(Throwable caught) {
						HTML message = new HTML(caught.getMessage());
						vp.add(message);
					}

					@Override
					public void onSuccess(String result) {						

						final DialogBox messaggio = new DialogBox();
						messaggio.setText(result);
						messaggio.setPopupPosition(250, 250);
						VerticalPanel vPanel = new VerticalPanel();

						vPanel.add(new Button("Chiudi", new ClickHandler() {
							public void onClick(ClickEvent event) {
								vp.clear();
								HomeAdm home = new HomeAdm(vp);
								home.onModuleLoad();
								messaggio.hide();	
							}
						}));
						messaggio.setWidget(vPanel);		
						messaggio.show();
					}
				});				
			}			
		});
		
		//eliminazione offerta
		final Button btn2 = new Button("ELIMINA OFFERTA");
		btn2.setEnabled(false);
		
		//RIEMPIMENTO TABELLA OFFERTE
		HTML title = new HTML("<h3>SELEZIONA OFFERTA DA RIMUOVERE</h3>");
		

		ArrayList <Offerta> offerte = new ArrayList<Offerta>();
		for(int i=0; i<oggetto.getOfferta().size(); i++) {
			offerte.add(oggetto.getOfferta().get(i));
		}
		//crea una cellTable
		final CellTable<Offerta> tableOfferte = new CellTable<Offerta>(500);
		tableOfferte.setKeyboardSelectionPolicy (KeyboardSelectionPolicy.ENABLED);

		//colonna username offerente
		TextColumn<Offerta> offerenteColumn = new TextColumn<Offerta>() {
			@Override
			public String getValue(Offerta o) {
				return o.getOfferente().getUsername();
			}
		};
		tableOfferte.addColumn(offerenteColumn, "OFFERENTE");

		//colonna importo double offerta
		TextColumn<Offerta> importoColumn = new TextColumn<Offerta>() {
			@Override
			public String getValue(Offerta o) {
				return String.valueOf(o.getImporto());
			}
		};
		tableOfferte.addColumn(importoColumn, "IMPORTO");
		
		//colonna stato offerta
		TextColumn<Offerta> statoColumn = new TextColumn<Offerta>() {
			@Override
			public String getValue(Offerta o) {
				return o.getStato();
			}
		};
		tableOfferte.addColumn(statoColumn, "STATO OFFERTA");

		tableOfferte.setWidth("90%", true);
		tableOfferte.setColumnWidth(offerenteColumn, 20.0 , Unit.PCT);
		tableOfferte.setColumnWidth(importoColumn, 20.0 , Unit.PCT);
		tableOfferte.setColumnWidth(statoColumn, 20.0 , Unit.PCT);
		
		tableOfferte.setRowCount(offerte.size(), true);
		tableOfferte.setRowData(0, offerte);//inserimento arraylist di offerte nella tabella

		final SingleSelectionModel<Offerta> selectionModel = new SingleSelectionModel<Offerta>();
		tableOfferte.setSelectionModel(selectionModel);
		selectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
			public void onSelectionChange(SelectionChangeEvent event) {
				offertaSelezionata = selectionModel.getSelectedObject();
					btn2.setEnabled(true);		
			}
		});
		
		//handler eliminazione offerta
		btn2.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				 GreetingServiceAsync greet = GWT.create(GreetingService.class);
				 greet.eliminaOfferta(offertaSelezionata, oggetto, new AsyncCallback<Oggetto>() {

					@Override
					public void onFailure(Throwable caught) {
						HTML message= new HTML(caught.getMessage());
						vp.add(message);	
					}

					@Override
					public void onSuccess(Oggetto result) {
						
						final Oggetto oggettoAggiornato = result;
						final DialogBox messaggio = new DialogBox();
						messaggio.setText("Offerta rimossa con successo!");
						messaggio.setPopupPosition(250, 200);
						VerticalPanel vPanel = new VerticalPanel();

						vPanel.add(new Button("Chiudi", new ClickHandler() {
							public void onClick(ClickEvent event) {
								vp.clear();
								OggettoAdminView paginacorrente = new OggettoAdminView(vp, oggettoAggiornato);
								paginacorrente.onModuleLoad();		

								messaggio.hide();
							}
						}));
						messaggio.setWidget(vPanel);		
						messaggio.show();
					}					 
				 });				
			}			
		});
		
		//eliminazione domanda/risposta
		HTML title2 = new HTML("<h3>SELEZIONA DOMANDA O RISPOSTA DA RIMUOVERE</h3>");
		
		final Button btn3= new Button("ELIMINA DOMANDA");
		final Button btn4 = new Button("ELIMINA RISPOSTA");
		btn3.setEnabled(false);
		btn4.setEnabled(false);

		//RIEMPIMENTO TABELLA DI DOMANDE E RISPOSTE
		ArrayList <Domanda> domandeERisposte = new ArrayList<Domanda>();
		for(int i=0; i<oggetto.getDomande().size(); i++) {
			domandeERisposte.add(oggetto.getDomande().get(i));
		}
		//crea una cellTable
		final CellTable<Domanda> table = new CellTable<Domanda>(500);
		table.setKeyboardSelectionPolicy (KeyboardSelectionPolicy.ENABLED);

		//colonna testo domanda.
		TextColumn<Domanda> textColumn = new TextColumn<Domanda>() {
			@Override
			public String getValue(Domanda d) {
				return d.getTesto();
			}
		};
		table.addColumn(textColumn, "DOMANDE degli UTENTI");

		//colonna username domandante.
		TextColumn<Domanda> usernameColumn = new TextColumn<Domanda>() {
			@Override
			public String getValue(Domanda d) {
				return d.getMittente().getUsername();
			}
		};
		table.addColumn(usernameColumn, "Username");
		
		//colonna testo risposta
		TextColumn<Domanda> rispostaColumn = new TextColumn<Domanda>() {
			@Override
			public String getValue(Domanda d) {
				if(d.getRisposta().getTesto()!=null) {
					return d.getRisposta().getTesto();
				}else {
					return "Risposta non inserita";
				}
			}
		};
		table.addColumn(rispostaColumn, "RISPOSTA del VENDITORE");

		//colonna timestamp domanda.
		TextColumn<Domanda> timeColumn = new TextColumn<Domanda>() {
			@Override
			public String getValue(Domanda d) {
				return d.getTimestamp().toString();
			}
		};
		table.addColumn(timeColumn, "dettagli");

		table.setWidth("90%", true);
		table.setColumnWidth(textColumn, 20.0 , Unit.PCT);
		table.setColumnWidth(usernameColumn, 20.0 , Unit.PCT);
		table.setColumnWidth(timeColumn, 20.0 , Unit.PCT);
		table.setColumnWidth(rispostaColumn, 20.0 , Unit.PCT);

		table.setRowCount(domandeERisposte.size(), true);
		table.setRowData(0, domandeERisposte);// Push the data into the widget.


		final SingleSelectionModel<Domanda> selection = new SingleSelectionModel<Domanda>();
		table.setSelectionModel(selection);
		selection.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
			public void onSelectionChange(SelectionChangeEvent event) {
				domandaSelezionata = selection.getSelectedObject();
				
					btn3.setEnabled(true);
					btn4.setEnabled(true);
				
			}
		});
		
		//handler eliminazione risposta
		btn4.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				GreetingServiceAsync greetingservice= GWT.create(GreetingService.class);
				greetingservice.eliminaRisposta(domandaSelezionata, oggetto , new AsyncCallback<Oggetto>() {

					@Override
					public void onFailure(Throwable caught) {
						HTML message= new HTML(caught.getMessage());
						vp.add(message);	
					}

					@Override
					public void onSuccess(Oggetto result) {

						final Oggetto obj = result;
						final DialogBox messaggio = new DialogBox();
						messaggio.setText("Risposta rimossa con successo!");
						messaggio.setPopupPosition(250, 200);
						VerticalPanel vPanel = new VerticalPanel();

						vPanel.add(new Button("Chiudi", new ClickHandler() {
							public void onClick(ClickEvent event) {
								vp.clear();
								OggettoAdminView paginacorrente = new OggettoAdminView(vp, obj);
								paginacorrente.onModuleLoad();		

								messaggio.hide();
							}
						}));
						messaggio.setWidget(vPanel);		
						messaggio.show();
					}					
				});							
			}
		});
		
		//handler eliminazione domanda
		btn3.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				GreetingServiceAsync greetingservice= GWT.create(GreetingService.class);
				greetingservice.eliminaDomanda(oggetto, domandaSelezionata, new AsyncCallback<Oggetto>() {

					@Override
					public void onFailure(Throwable caught) {
						HTML message= new HTML(caught.getMessage());
						vp.add(message);	
					}

					@Override
					public void onSuccess(Oggetto result) {

						final Oggetto oggettorisultante = result;
						final DialogBox messaggio = new DialogBox();
						messaggio.setText("Domanda rimossa con successo!");
						messaggio.setPopupPosition(250, 200);
						VerticalPanel vPanel = new VerticalPanel();

						vPanel.add(new Button("Chiudi", new ClickHandler() {
							public void onClick(ClickEvent event) {
								vp.clear();
								OggettoAdminView paginacorrente = new OggettoAdminView(vp, oggettorisultante);
								paginacorrente.onModuleLoad();		

								messaggio.hide();
							}
						}));
						messaggio.setWidget(vPanel);		
						messaggio.show();
					}					
				});							
			}
		});
		vp.add(title);
		vp.add(tableOfferte);
		vp.add(new HTML("<br>"));
		vp.add(title2);
		vp.add(table);
		vp.add(new HTML("<br>"));
		
		hp.add(btn1);
		hp.add(btn2);
		hp.add(btn3);
		hp.add(btn4);
		vp.add(hp);
		RootPanel.get().add(vp);
	}

}
