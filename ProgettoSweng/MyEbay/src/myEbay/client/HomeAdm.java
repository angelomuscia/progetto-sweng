package myEbay.client;

import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

public class HomeAdm {
	
	private VerticalPanel mainPanel = new VerticalPanel();
	Comando cmd = new Comando(mainPanel);
	
	public HomeAdm(VerticalPanel vp){
		this.mainPanel = vp;
		
	}

	public void onModuleLoad()	{
		
		NavBarChoose menuAmministratore = new NavBarChoose(mainPanel, 3);
		menuAmministratore.onModuleLoad();
		
		mainPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		HTML descrizione = new HTML("<h2>HomePage Amministratore</h2>");
		mainPanel.add(descrizione);
		
		Catalogo catalogo = new Catalogo(mainPanel);
		catalogo.onModuleLoad();
		
		mainPanel.setStyleName("center");
		RootPanel.get().add(mainPanel);
	}
}
