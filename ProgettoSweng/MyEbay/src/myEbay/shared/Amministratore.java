package myEbay.shared;

public class Amministratore implements Utente {

	private static final long serialVersionUID = 1L;
	
	private String username;
	private String password;
	private String email;
	
	public Amministratore() {}
	//costruttore
	public Amministratore(String username, String password, String email) {
		this.username= username;
		this.password= password; 
		this.email= email;
	}
	
	//get
	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public String getEmail() {
		return email;
	}

    

	
	public void eliminaOfferta(Offerta offerta){};
	
	public void eliminaOggetto(Oggetto oggetto) {};
	
	public Categoria aggiungiCategoria(String nomeCategoria) {
		return null;
	};
	
	public void eliminaCategoria(String nomeCategoria) {};
	public void rinominaCategoria(String nomeCategoria, Categoria categoria) {};
	

}
