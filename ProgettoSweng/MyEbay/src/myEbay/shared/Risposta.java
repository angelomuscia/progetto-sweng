package myEbay.shared;

import java.io.Serializable;
import java.sql.Timestamp;

public class Risposta implements Serializable  {

	/**
	 * mittente e timestamp chiavi primarie
	 */
	private static final long serialVersionUID = 1L;
	private String testo;
	private UtenteRegistrato venditore;
	private Timestamp timestamp;
	
	//costruttore senza parametri
	public Risposta() {}
	
	//costruttore con parametri
	public Risposta(String testo, UtenteRegistrato mittente) {
		this.testo = testo;
		this.venditore = mittente;
		this.timestamp = new Timestamp(System.currentTimeMillis());
	}
	
	public String getTesto() {
		return testo;
	}
	
	public void setTesto(String testo) {
		this.testo = testo;
	}
	
	public UtenteRegistrato getRispondente() {
		return venditore;
	}
	
	public void setRispondente(UtenteRegistrato venditore) {
		this.venditore = venditore;
	}
	
	public Timestamp getTimestamp() {
		return timestamp;
	}	
}

