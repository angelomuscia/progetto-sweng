package myEbay.shared;

import java.io.Serializable;

public enum StatoAsta  implements Serializable {

		ASTAINCORSO("Asta in corso  e oggetto non aggiudicato"),
		ASTACONCLUSA("Asta conclusa e oggetto aggiudicato"), 
		ASTASCADUTA("Asta conclusa e oggetto non aggiudicato");
	
		private String stato;

		private StatoAsta(String stato) {
			this.stato = stato;
		}

		public String getStatoAsta() {
			return stato;
		}

}


