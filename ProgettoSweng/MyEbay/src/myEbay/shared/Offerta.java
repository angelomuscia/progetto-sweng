package myEbay.shared;

import java.io.Serializable;

public class Offerta implements Serializable{
	
	/**
	 * Classe per la definizione di un UtenteRegistrato
	 * 
	 * @author Angela Convertino, Elisa Andriani, Angelo Muscia Masuzzo
	 */
	private static final long serialVersionUID = 1L;
	private double importo;
	private UtenteRegistrato offerente;
	private String stato;	
	
	// STATO OFFERTA PU� ESSERE: 
	/* asta chiusa e oggetto aggiudicato
	 * asta chiusa e oggetto non aggiudicato
	 * asta in corso e offerta migliore
	 * asta in corso e offerta superata
	 */
	
	public Offerta() {
		this.importo = 0.0;
	}

	public Offerta(UtenteRegistrato offerente, double importo) {
		this.offerente = offerente;
		this.importo = importo;
	}
	
	//get 
	public UtenteRegistrato getOfferente() {
		return this.offerente;
	}

	public double getImporto() {
		return this.importo;
	}

	public String getStato() {
		return this.stato;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	// set
	public void setStato(StatoOfferta stato) {
		this.stato = stato.getStato();
	}

	public void setImporto(double importo) {
		this.importo = importo;
	}

	public void setOfferente(UtenteRegistrato offerente) {
		this.offerente = offerente;
	}

	public void setStato(String stato) {
		this.stato = stato;
	}
}
