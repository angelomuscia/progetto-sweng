package myEbay.shared;

import java.io.Serializable;

public enum StatoOfferta implements Serializable {
	
	OGGETTOAGGIUDICATO("Asta chiusa e oggetto aggiudicato"), OGGETTONONAGGIUDICATO("Asta chiusa e oggetto non aggiudicato"),
	OFFERTMIGLIORE("Asta in corso e offerta migliore"), OFFERTASUPERATA("Asta in corso e offerta superata");

	private String stato;

	private StatoOfferta(String stato) {
		this.stato = stato;
	}

	public String getStato() {
		return stato;
	}

}
