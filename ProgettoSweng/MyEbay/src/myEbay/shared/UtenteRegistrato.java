package myEbay.shared;

/**
 * Classe per la definizione di un UtenteRegistrato
 * 
 * @author Angela Convertino, Elisa Andriani, Angelo Muscia Masuzzo
 */
public class UtenteRegistrato implements Utente{
	
	private static final long serialVersionUID = 1L;
	
	private String username;
	private String password;
	private String email;
	private String nome;
	private String cognome;
	private String indirizzo;
	private String numeroTelefono;
	private String codiceFiscale;
	private String sesso;
	private String luogoNascita;
	private String dataNascita;
	
	/*
	 * Costruttore
	 * @param username di tipo String
	 * @param nome di tipo String
	 * @param cognome di tipo String
	 * @param indirizzo di tipo String
	 * @param numeroTelefono di tipo String
	 * @param email di tipo String
	 * @param password di tipo String
	 * @param codiceFiscale di tipo String 
	 * @param sesso di tipo String
	 * @param luogoNascita di tipo String
	 * @param dataNascita di tipo String
	 */
	public UtenteRegistrato() {}
	public UtenteRegistrato(String username,String nome, String cognome,  String indirizzo, String numeroTelefono, String email, 
			String password, String codiceFiscale, String sesso, String luogoNascita, String dataNascita) {
		super();
		this.username=username;
		this.password=password;
		this.email=email;
		this.nome = nome;
		this.cognome = cognome;
		this.codiceFiscale = codiceFiscale;
		this.indirizzo = indirizzo;
		this.numeroTelefono = numeroTelefono;
		this.sesso = sesso;
		this.luogoNascita = luogoNascita;
		this.dataNascita = dataNascita;
	}

	// GET E SET 
	/**
	 * Estrattore Username UtenteRegistrato
	 * @return Username di tipo String
	 */
	public String getUsername() {
		return this.username;
	}
	
	/**
	 * Estrattore nome UtenteRegistrato
	 * @return nome di tipo String
	 */
	public String getNome() {
		return this.nome;
	}
	
	/**
	 * Estrattore cognome UtenteRegistrato
	 * @return cognome di tipo String
	 */
	public String getCognome() {
		return this.cognome;
	}

	/**
	 * Estrattore codiceFiscale UtenteRegistrato
	 * @return codiceFiscale di tipo String
	 */
	public String getCodiceFiscale() {	
		return this.codiceFiscale;
	}
	
	/**
	 * Estrattore indirizzo UtenteRegistrato
	 * @return indirizzo di tipo String
	 */
	public String getIndirizzo() {
		return this.indirizzo;
	}
	
	/**
	 * Estrattore numeroTelefono UtenteRegistrato
	 * @return numeroTelefono di tipo String
	 */
	public String getNumeroTelefono() {
		return this.numeroTelefono;
	}
	
	/**
	 * Estrattore email UtenteRegistrato
	 * @return email di tipo String
	 */
	public String getEmail() {
		return this.email;
	}
	
	/**
	 * Estrattore password UtenteRegistrato
	 * @return password di tipo String
	 */
	public String getPassword() {
		return this.password;
	}
	
	/**
	 * Estrattore sesso UtenteRegistrato
	 * @return sesso di tipo String
	 */
	public String getSesso() {
		return this.sesso;
	}
	
	/**
	 * Estrattore luogoNascita UtenteRegistrato
	 * @return luogoNascita di tipo String
	 */
	public String getLuogoNascita() {
		return this.luogoNascita;
	}
	
	/**
	 * Estrattore dataNascitaNascita UtenteRegistrato
	 * @return dataNascita di tipo String
	 */
	public String getDataNascita() {
		return this.dataNascita;
	}
	
	
	/**
	 * Metodo che stampa tutti i dati dell'UtenteRegistrato
	 * @return tutti i dati
	 */
	@Override
	public String toString() {
		return "UtenteRegistrato [username=" + username + ", nome=" + nome + ", cognome=" + cognome + ", codiceFiscale="
				+ codiceFiscale + ", indirizzo=" + indirizzo + ", numeroTelefono=" + numeroTelefono + ", email=" + email
				+ ", password=" + password + ", sesso=" + sesso + ", luogoNascita=" + luogoNascita + ", dataNascita="
				+ dataNascita + "]";
	}
	
}
