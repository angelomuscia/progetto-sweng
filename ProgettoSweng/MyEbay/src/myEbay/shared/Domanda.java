package myEbay.shared;

import java.io.Serializable;
import java.sql.Timestamp;

public class Domanda implements Serializable {
	
		/**
		 * mittente e timestamp chiavi primarie
		 */
		private static final long serialVersionUID = 1L;
		private String testo;
		private UtenteRegistrato mittente;
		private Risposta risposta;
		private Timestamp timestamp;

		//costruttore senza parametri
		public Domanda() {}
		
		//costruttore con parametri
		public Domanda(String testo, UtenteRegistrato mittente) {
			this.testo = testo;
			this.mittente = mittente;
			this.risposta =  new Risposta();
			this.timestamp = new Timestamp(System.currentTimeMillis());
		}
		
		public String getTesto() {
			return testo;
		}
		
		
		public UtenteRegistrato getMittente() {
			return mittente;
		}
		
		
		public Timestamp getTimestamp() {
			return timestamp;
		}

		public Risposta getRisposta() {
			return risposta;
		}

		public void setRisposta(String testo, UtenteRegistrato mittente) {
			risposta = new Risposta(testo , mittente);
		}
}

