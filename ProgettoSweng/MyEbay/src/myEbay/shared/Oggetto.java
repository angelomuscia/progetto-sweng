package myEbay.shared;

import java.io.Serializable;
import java.util.LinkedList;

public class Oggetto implements Serializable{

	/**
	 * Classe per la definizione di un UtenteRegistrato
	 * 
	 * @author Angela Convertino, Elisa Andriani, Angelo Muscia Masuzzo
	 */
	private static final long serialVersionUID = 1L;
	private String nome;
	private String descrizione;
	private double baseAsta;
	private String scadenza;
	private Categoria categoria;
	private String venditore;
	private LinkedList<Offerta> offerta;
	private LinkedList<Domanda> domande;
	private String stato;

	/*
	 * Costruttore
	 * */
	public Oggetto() {
	}
	
	/*
	 * Costruttore
	 * @param venditore di tipo UtenteRegistrato
	 * @param nome di tipo String
	 * @param descrizione di tipo String
	 * @param baseAsta di tipo double
	 * @param scadenza di tipo String
	 * @param categoria di tipo Categoria
	 */
	public Oggetto(String venditore, String nome, String descrizione, double baseAsta, String scadenza,
			Categoria categoria) {
		this.venditore = venditore;
		this.nome = nome;
		this.descrizione = descrizione;
		this.baseAsta= baseAsta;
		this.scadenza = scadenza;
		this.categoria = categoria;
		this.domande = new LinkedList<>();
		this.offerta = new LinkedList<>();
		
	}
	public void addDomanda (Domanda domanda) {
		domande.add(domanda);
	}
	// GET nome, descrizione, base asta, scadenza asta, categoria, venditore, offerta, messaggio, stato
	public String getNome() {
		return nome;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public double getBaseAsta() {
		return baseAsta;
	}

	public String getScadenza() {
		return scadenza;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public String getVenditore() {
		return venditore;
	}

	public LinkedList<Offerta> getOfferta() {
		return offerta;
	}

	public LinkedList<Domanda> getDomande() {
		return domande;
	}

	public String getStato() {
		return stato;
	}


	// SET nome, descrizione, base asta, scadenza asta, categoria, venditore, offerta, messaggio, stato
	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public void setBaseAsta(double baseAsta) {
		this.baseAsta = baseAsta;
	}

	public void setScadenza(String scadenza) {
		this.scadenza = scadenza;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public void setVenditore(String venditore) {
		this.venditore = venditore;
	}

	public void setOfferta(Offerta offerta) {
		this.offerta.add(offerta);
	}
	public String getKey() {
		return venditore+" "+nome+" "+scadenza;
	}
	
	public void setStato(StatoAsta stato) {
		this.stato = stato.getStatoAsta();
	}
	
	public double getOffertaMax() {
		double max = baseAsta;
		if(offerta.size()>0)
			return offerta.getLast().getImporto();
		return max;
	}
}
