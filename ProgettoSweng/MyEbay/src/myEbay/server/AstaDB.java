package myEbay.server;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Map.Entry;

import org.mapdb.BTreeMap;
import org.mapdb.DB;

import com.ibm.icu.text.SimpleDateFormat;

import myEbay.shared.Categoria;
import myEbay.shared.Offerta;
import myEbay.shared.Oggetto;
import myEbay.shared.StatoAsta;
import myEbay.shared.StatoOfferta;
import myEbay.shared.Utente;

public class AstaDB {
	
	private static void clearDB() {
		DB db = DBConfig.getInstance();
		@SuppressWarnings("unchecked")
		BTreeMap<String, Utente> mapOggetti = (BTreeMap<String, Utente>) db.treeMap("mapOggetti").createOrOpen();
		mapOggetti.clear();
		db.commit();
		db.close();
	}

	//metodo inserimento oggetto in asta
	@SuppressWarnings("unchecked")
	public static String inserimentoAsta(String venditore, String nome, String categoria, 
			double baseAsta, String scadenza, String descrizione ) throws IllegalArgumentException {
		
		DB db = DBConfig.getInstance();
		
		BTreeMap<String, Oggetto> mapOggetti = (BTreeMap<String, Oggetto>) db.treeMap("mapOggetti").createOrOpen();	
		BTreeMap<String, Categoria> mapCategorie = (BTreeMap<String, Categoria>) db.treeMap("mapCategorie").createOrOpen();
		Categoria categ = mapCategorie.get(categoria);
		
		Oggetto oggettoDaVendere = new Oggetto(venditore, nome, descrizione, baseAsta, scadenza, categ);
		oggettoDaVendere.setStato(StatoAsta.ASTAINCORSO);
		String id = venditore+" "+nome+" "+scadenza;		
		Date today = new Date();
		Date dataScadenza = today;
		try {
			dataScadenza = new SimpleDateFormat("dd/MM/yyyy").parse(scadenza);
		} catch (ParseException e) {
			
			throw new IllegalArgumentException("Formato data errato");
		}
		
		//controllo data scadenza asta
		if (dataScadenza.after(today)) {
			mapOggetti.put(id,oggettoDaVendere);
			db.commit();	
		}else {
			throw new IllegalArgumentException("Occorre data di scadenza successiva a quella corrente");
		}
		
		//aggiornamento mapCategoria
	
		categ.setOggetti(oggettoDaVendere);
		mapCategorie.put(categ.getNome(), categ);
		db.commit();
		db.close();
		
		return "Oggetto inserito con successo";
	} 
	
	//metodo che restituisce gli oggetti in vendita e setta gli stati.
	/*STATO OGGETTO PUò ESSERE: 
		/* asta in corso
		 * asta conclusa e oggetto aggiudicato da…
		 * asta conclusa e oggetto non aggiudicato */
	
	public static ArrayList<Oggetto> getOggetti(){
		//clearDB();//svuoto il db degli oggetti
		Date today = new Date();
		ArrayList<Oggetto> lista = new ArrayList<Oggetto>(); //lista oggetti in vendita da restituire
		DB db = DBConfig.getInstance();
		@SuppressWarnings("unchecked")
		BTreeMap<String, Categoria> mapCategorie = (BTreeMap<String, Categoria>) db.treeMap("mapCategorie").createOrOpen();
		//mapCategorie.clear();
		@SuppressWarnings("unchecked")
		BTreeMap<String, Oggetto> mapOggetti = (BTreeMap<String, Oggetto>) db.treeMap("mapOggetti").createOrOpen();	
		
		for(Entry<String, Oggetto> entry : mapOggetti.entrySet()) {
			Date scadenza = new Date();
			try {
				scadenza = new SimpleDateFormat("dd/MM/yyyy").parse(entry.getValue().getScadenza());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(scadenza.before(today)|| scadenza==today) {
				if(entry.getValue().getOfferta().size()>0) {
					entry.getValue().setStato(StatoAsta.ASTACONCLUSA); 
					for(Offerta offerta : entry.getValue().getOfferta()) {
						if(offerta.getStato().equals(StatoOfferta.OFFERTMIGLIORE)) {
							offerta.setStato(StatoOfferta.OGGETTOAGGIUDICATO);
							
						}
						else if(offerta.getStato().equals(StatoOfferta.OFFERTASUPERATA)){
							offerta.setStato(StatoOfferta.OGGETTONONAGGIUDICATO);	
						}
					}
				}
				else entry.getValue().setStato(StatoAsta.ASTASCADUTA);
				
				mapOggetti.put(entry.getValue().getKey(), entry.getValue());
				
				Categoria categoria = entry.getValue().getCategoria();
				//toglie l'oggetto dall'arraylist di oggetti nella categoria corrispondente
				ArrayList<Oggetto> oggettiInCategoria = categoria.getOggetti();
				for(int i=0; i<oggettiInCategoria.size(); i++) {
					if(oggettiInCategoria.get(i).getKey().equals(entry.getValue().getKey())) {
						oggettiInCategoria.remove(i);
						mapCategorie.put(categoria.getNome(), categoria);
						break;		
					}	
				}	
			}
			else {
				lista.add(entry.getValue());
			}	
		}
		db.commit();
		db.close();
		ordinaOggetti(lista);
		return lista;
	}
	//metodo che restituisce gli oggetti messi in vendita da un utente
	public static ArrayList<Oggetto> myObjs(String utente){
		ArrayList<Oggetto> lista = new ArrayList<Oggetto>();
		getOggetti(); //chiamo getOggetti per aggiornare gli stati
		DB db = DBConfig.getInstance();
		@SuppressWarnings("unchecked")
		BTreeMap<String, Oggetto> mapOggetti = (BTreeMap<String, Oggetto>) db.treeMap("mapOggetti").createOrOpen();	
		for(Oggetto c : mapOggetti.values()) {
			try {
				if(c.getVenditore().equals(utente))
					lista.add(c);
			}catch (NullPointerException e) {
				continue;
			}
		}
		return lista;	
	}
	
	
	//metodo che  mette in ordine cronologico dall’oggetto con scadenza più vicina.
	private static ArrayList<Oggetto> ordinaOggetti(ArrayList<Oggetto> oggettidaOrdinare) {
		Collections.sort(oggettidaOrdinare, new Comparator<Oggetto>() {
			@Override
			public int compare(Oggetto o1, Oggetto o2) {
				Date data1 = null; 
				Date data2 = null;
				try {
					data1 = new SimpleDateFormat("dd/MM/yyyy").parse(o1.getScadenza());
					data2 = new SimpleDateFormat("dd/MM/yyyy").parse(o2.getScadenza());
				} catch (ParseException e) {
					e.printStackTrace();
				} 
				return data1.compareTo(data2);
			}
		});
		return oggettidaOrdinare;
	}
	
	/*FILTRO OGGETTI in Vendita
	 * @Param categoria
	 * @return array di Oggetti
	*/
	@SuppressWarnings("unchecked")
	public static ArrayList<Oggetto> oggettiFiltrati(String categoria){
		
		ArrayList<Oggetto> catalogoFiltrato = new ArrayList<Oggetto>();
		ArrayList<Categoria> sottocategorie = null;
		
		if(categoria.equals("--SELEZIONA CATEGORIA--")) {
			throw new IllegalArgumentException("Nessuna categoria selezionata");
		}
		
		DB db = DBConfig.getInstance();
		BTreeMap<String, Categoria> mapCategoria = (BTreeMap<String, Categoria>) db.treeMap("mapCategorie").createOrOpen();
		
		catalogoFiltrato.addAll(mapCategoria.get(categoria).getOggetti());
		sottocategorie = new ArrayList<Categoria>();
		for(Categoria categ : sottocategorie) {
			catalogoFiltrato = oggettiDiSottocategorie(categ,catalogoFiltrato);
		}
		return ordinaOggetti(catalogoFiltrato);
	}
	
	private static ArrayList<Oggetto> oggettiDiSottocategorie(Categoria categ, ArrayList<Oggetto> catalogoFiltrato){
		

		DB db = DBConfig.getInstance();
		@SuppressWarnings("unchecked")
		BTreeMap<String, Categoria> mapCategoria = (BTreeMap<String, Categoria>) db.treeMap("mapCategorie").createOrOpen();
		Categoria sottocategoria = mapCategoria.get(categ.getNome());
		catalogoFiltrato.addAll(sottocategoria.getOggetti());
		for(Categoria c : sottocategoria.getSottocategorie()) {
			catalogoFiltrato = oggettiDiSottocategorie(c, catalogoFiltrato);
		}
		return catalogoFiltrato;
	}
	
}

