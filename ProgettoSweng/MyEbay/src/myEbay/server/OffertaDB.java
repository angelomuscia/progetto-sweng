package myEbay.server;

import java.util.ArrayList;

import org.mapdb.BTreeMap;
import org.mapdb.DB;

import myEbay.shared.Offerta;
import myEbay.shared.Oggetto;
import myEbay.shared.StatoOfferta;
import myEbay.shared.Utente;
import myEbay.shared.UtenteRegistrato;
// STATO OFFERTA PU� ESSERE: 
	/* asta chiusa e oggetto aggiudicato
	 * asta chiusa e oggetto non aggiudicato
	 * asta in corso e offerta migliore
	 * asta in corso e offerta superata
	 */
public class OffertaDB {

	/**
	 * Metodo che aggiunge un'offerta al DB
	 * @param offerta
	 * @return stringa stampa
	 */
	@SuppressWarnings({ "unused", "unchecked" })
	public static String doOfferta(double importo, String offerente, Oggetto oggetto) {
	    DB db = DBConfig.getInstance();
		BTreeMap<String, Oggetto> mapOggetto = (BTreeMap<String, Oggetto>) db.treeMap("mapOggetti").createOrOpen();	
		BTreeMap<String, Utente> mapUtente = (BTreeMap<String, Utente>) db.treeMap("mapUtenti").createOrOpen();	
		UtenteRegistrato u = (UtenteRegistrato) mapUtente.get(offerente);
		
		String stampa=null;
		
		if(offerente!="" & importo>0.0) {
			if(importo> oggetto.getBaseAsta()){
				Offerta offertaDaInserire = new Offerta(u, importo);
				if(checkOfferta(offertaDaInserire, oggetto)) {
					Oggetto o = mapOggetto.get(oggetto.getKey());	
					Offerta superata = getOffertaMax(o);
					if(superata.getOfferente()==null || !superata.getOfferente().getUsername().equals(u.getUsername())) {
						superata.setStato(StatoOfferta.OFFERTASUPERATA);
						offertaDaInserire.setStato(StatoOfferta.OFFERTMIGLIORE);
						o.setOfferta(offertaDaInserire);
						mapOggetto.put(o.getKey(), o);
						db.commit();
						db.close();
						stampa= "Offerta aggiunta con successo.|" + offertaDaInserire.getImporto();
						
					} else stampa= "Sei gi� il miglior offerente!";
				}    
				else stampa= "ERRORE: Hai fatto un'offerta minore di quella attuale.";
			}
			else stampa="ERRORE: Importo inserito inferiore alla base d'asta.";
		}
		else stampa="ERRORE: Importo non valido." ;
		
	return stampa;
	}
	
	//metodo per verificare che un'offerta sia presente all'interno dell'oggetto considerato
	private static boolean checkOfferta(Offerta offertaDaVerificare, Oggetto oggetto){
		DB db = DBConfig.getInstance();
		@SuppressWarnings("unchecked")
		BTreeMap<String, Oggetto> mapOggetto = (BTreeMap<String, Oggetto>) db.treeMap("mapOggetti").createOrOpen();	
		Oggetto obj = mapOggetto.get(oggetto.getKey());
		Offerta temp = getOffertaMax(obj);
		
		if(offertaDaVerificare.getImporto() > temp.getImporto()) {
			return true;		
		}
		else return false;
}

	private static Offerta getOffertaMax(Oggetto oggetto) {
		Offerta temp = new Offerta();

		for(Offerta offerta : oggetto.getOfferta()) {
			if(temp.getImporto()<offerta.getImporto())
				temp= offerta;
		}
		return temp;
	}
	
	public static ArrayList<Oggetto> mieOfferte (String utente){
		AstaDB.getOggetti();//Aggiornamento DB
		ArrayList<Oggetto> lista = new ArrayList<Oggetto>();
		DB db = DBConfig.getInstance();
		@SuppressWarnings("unchecked")
		BTreeMap<String, Oggetto> mapOggetto = (BTreeMap<String, Oggetto>) db.treeMap("mapOggetti").createOrOpen();
		@SuppressWarnings("unchecked")
		BTreeMap<String, Utente> mapUtente = (BTreeMap<String, Utente>) db.treeMap("mapUtenti").createOrOpen();	
		UtenteRegistrato u = (UtenteRegistrato) mapUtente.get(utente);	

		for(Oggetto oggetto: mapOggetto.values()) {
			for(Offerta offerta : oggetto.getOfferta()) {
				if(offerta.getOfferente().getUsername().equals(u.getUsername())) {
					lista.add(oggetto);
					break;
				}
			}
		}
		return lista;
	}

	
}
	

	

/*      @SuppressWarnings("unchecked")
		public static void cambiaStato(Oggetto oggettoConsiderato){
		DB db = DBConfig.getInstance();
		BTreeMap<String, Oggetto> mapOggetto = (BTreeMap<String, Oggetto>) db.treeMap("mapOggetto").createOrOpen();	Timestamp oggi = new Timestamp(System.currentTimeMillis());
        Offerta offertaVincitrice=null;

        SimpleDateFormat astaScadenzaOggetto = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
		Date scadenzaAstaOggetto = astaScadenzaOggetto.parse(oggettoConsiderato.getScadenza());
		Timestamp timeStampDate = new Timestamp(scadenzaAstaOggetto.getTime());

		DB db = DBConfig.getInstance();
		BTreeMap<String, Oggetto> mapOggetto =(BTreeMap<String, Oggetto>) db.treeMap("mapOggetto").createOrOpen();
			
		// controllo se mapOggetto � piena, se si: cerco le offerte per un determinato oggetto
		if(mapOggetto.isEmpty()==false) {
			for(Entry<String, Oggetto> entry : mapOggetto.entrySet()) {
				if(entry.getValue().equals(oggettoConsiderato)) {
					// se scadenza � uguale alla data attuale allora setto stato asta a conclusa
					if(timeStampDate.after(oggi)) {
						
						//se l'oggetto non ha ricevuto offerte
						if(entry.getValue().getOfferta().isEmpty())
							entry.getValue().setStato(StatoAsta.ASTASCADUTA);
						
						else {// se LinkedList di offerte ha almeno 1 offerta
							entry.getValue().setStato(StatoAsta.ASTACONCLUSA);
							for(int i=0; i<entry.getValue().getOfferta().size(); i++) {
								// se offerta vincitrice � nulla la setto alla prima offerta disponibile e chiudo stato dell'asta
								if(offertaVincitrice==null) 
									offertaVincitrice=entry.getValue().getOfferta().get(i);
								
								else // seleziono offerta migliore quando l'asta � gi� stata chiusa
									if(offertaVincitrice.getImporto()<entry.getValue().getOfferta().get(i).getImporto())
										offertaVincitrice=entry.getValue().getOfferta().get(i);	
							}
							// setto gli stati di tutte le offerte presenti per l'oggetto considerato
							for(int i=0; i<entry.getValue().getOfferta().size(); i++) {
								if(offertaVincitrice==entry.getValue().getOfferta().get(i))  
									entry.getValue().getOfferta().get(i).setStato("asta chiusa e oggetto aggiudicato");
								
								else
									entry.getValue().getOfferta().get(i).setStato("asta conclusa e oggetto non aggiudicato");
							}	
						}
						
					}else {
						entry.getValue().setStato(StatoAsta.ASTAINCORSO);
						for(int i=0; i<entry.getValue().getOfferta().size(); i++) {
							// se offerta vincitrice � nulla la setto alla prima offerta disponibile
							if(offertaVincitrice==null) 
								offertaVincitrice=entry.getValue().getOfferta().get(i);
							
							else // seleziono offerta migliore quando l'asta ha pi� offerte
								if(offertaVincitrice.getImporto()<entry.getValue().getOfferta().get(i).getImporto())
									offertaVincitrice=entry.getValue().getOfferta().get(i);	
						}
						// setto gli stati di tutte le offerte presenti per l'oggetto considerato
						for(int i=0; i<entry.getValue().getOfferta().size(); i++) {
							if(offertaVincitrice==entry.getValue().getOfferta().get(i))  
								entry.getValue().getOfferta().get(i).setStato("asta in corso e offerta migliore");
							
							else
								entry.getValue().getOfferta().get(i).setStato("asta in corso e offerta superata");
						}	
					}
				}
			}
		}
		// Scrittura e chiusura del db
		db.commit();
		db.close();*/

