package myEbay.server;

import java.util.ArrayList;
import org.mapdb.BTreeMap;
import org.mapdb.DB;

import myEbay.shared.Categoria;
import myEbay.shared.Domanda;
import myEbay.shared.Oggetto;

import myEbay.shared.Utente;
import myEbay.shared.UtenteRegistrato;

public class MessaggiDB {
	
	/**
	 * Metodo che aggiunge una domanda inerente ad un oggetto
	 * 
	 */
	@SuppressWarnings({ "unchecked" })
	public static Oggetto aggiungiDomanda(String testo, String mittente, Oggetto oggettoCommentato) {
		
		DB db = DBConfig.getInstance();
		
		BTreeMap<String, Oggetto> mapOggetto =(BTreeMap<String, Oggetto>) db.treeMap("mapOggetti").createOrOpen();	
		BTreeMap<String, Utente> mapUtente = (BTreeMap<String, Utente>) db.treeMap("mapUtenti").createOrOpen();	
		UtenteRegistrato u = (UtenteRegistrato) mapUtente.get(mittente);
		
		Domanda domanda = new Domanda(testo, u);
		Oggetto obj = mapOggetto.get(oggettoCommentato.getKey());
		obj.addDomanda(domanda); // Inserimento della domanda tra l'elenco delle domande per l'oggetto considerato
		mapOggetto.put(obj.getKey(), obj);
		
		BTreeMap<String, Categoria> mapCategorie = (BTreeMap<String, Categoria>) db.treeMap("mapCategorie").createOrOpen();
		Categoria categ = mapCategorie.get(obj.getCategoria().getNome());
		
		ArrayList<Oggetto> listaobjs = categ.getOggetti();
		for(int i=0; i<listaobjs.size(); i++) {
			if(listaobjs.get(i).getKey().equals(obj.getKey())) {
				listaobjs.remove(i);
				listaobjs.add(i, obj);
				mapCategorie.put(categ.getNome(), categ);
				break;		
			}	
		}
		
		db.commit();
		db.close();
		return obj;
		
	}	
			
	/**
	 * Metodo che aggiunge una risposta ad una domanda
	 * 
	 */
	@SuppressWarnings({ "unchecked" })
	public static Oggetto aggiungiRisposta(String testo, String venditore, Domanda domanda, Oggetto oggettoCommentato) {
		
	    DB db = DBConfig.getInstance();
		BTreeMap<String, Oggetto> mapOggetto =(BTreeMap<String, Oggetto>) db.treeMap("mapOggetti").createOrOpen();
		BTreeMap<String, Utente> mapUtente = (BTreeMap<String, Utente>) db.treeMap("mapUtenti").createOrOpen();	
		UtenteRegistrato u = (UtenteRegistrato) mapUtente.get(venditore);
		
		
		Oggetto obj = mapOggetto.get(oggettoCommentato.getKey());
		for(Domanda d : obj.getDomande())
		if(d.getTesto().equals(domanda.getTesto())) {
			d.setRisposta(testo, u);
			mapOggetto.put(obj.getKey(), obj);
			break;
		}
		
		BTreeMap<String, Categoria> mapCategorie = (BTreeMap<String, Categoria>) db.treeMap("mapCategorie").createOrOpen();
		Categoria categ = mapCategorie.get(obj.getCategoria().getNome());
		ArrayList<Oggetto> listaobjs = categ.getOggetti();
		for(int i=0; i<listaobjs.size(); i++) {
			if(listaobjs.get(i).getKey().equals(obj.getKey())) {
				listaobjs.add(i, obj);
				mapCategorie.put(categ.getNome(), categ);
				break;		
			}	
		}
		
		db.commit();
		db.close();
		return obj;
	}
	
}
