package myEbay.server;

import org.mapdb.BTreeMap;
import org.mapdb.DB;

import myEbay.shared.Amministratore;
import myEbay.shared.Utente;
import myEbay.shared.UtenteRegistrato;

public class Login {

	public static int doLogin (String username, String password) {
		DB db = DBConfig.getInstance();
		int soluzione=0;
	
		/* Crea mappa */
		@SuppressWarnings("unchecked")
		BTreeMap<String,Utente> mapUtenti = (BTreeMap<String, Utente>) db.treeMap("mapUtenti").createOrOpen();
		if(DBConfig.checkUtente(username)) { //verifico l'esistenza dell'utente
			Utente utente= mapUtenti.get(username);

			if(utente.getClass() == Amministratore.class) {
				Amministratore am = (Amministratore) utente;
				if(am.getPassword().contentEquals(password)) { //verifico la password
					soluzione=1; //amministratore ha effettuato il login
				}
				else soluzione=-1; //password errata	
			}
			else {
				UtenteRegistrato ur = (UtenteRegistrato) utente;
				if(ur.getPassword().contentEquals(password)) { //verifico la password
					soluzione=2;//utente ha effettuato il login	
				}
				else soluzione=-1; //password errata	
			}
		}
		else {
			soluzione=0; //utente non trovato
		}
		
		db.close();	
		return soluzione;
		
	}//fine doLogin

}
