package myEbay.server;
import java.util.ArrayList;
import java.util.Map.Entry;

import org.mapdb.BTreeMap;
import org.mapdb.DB;

import myEbay.shared.Categoria;
public class CategoriaDB {
	
	public static void clearDB() {
		DB db = DBConfig.getInstance();
		@SuppressWarnings("unchecked")
		BTreeMap<String, Categoria> mapCategoria = (BTreeMap<String, Categoria>) db.treeMap("mapCategorie").createOrOpen();
		mapCategoria.clear();
		db.commit();
		db.close();
	}
	
	/**
	 * Metodo che aggiunge una categoria al DB se non ancora esistente
	 * @param categoria
	 * @return stringa stampa con avviso
	 */
	@SuppressWarnings({ "unused", "unchecked" })
	public static String aggiungiCategoria(String padre, String nome ) {
	    DB db = DBConfig.getInstance();
		BTreeMap<String, Categoria> mapCategoria = (BTreeMap<String, Categoria>) db.treeMap("mapCategorie").createOrOpen();
		Categoria categoria = null;
		Categoria categoriaPadre = null;;
		String stampa = "";
		
		if(nome.isEmpty()) {
			stampa= "inserisci categoria";
			return stampa;
		}
		
		if(padre.isEmpty()) {
			categoria = new Categoria(nome);
		}
		else {
			if(mapCategoria.containsKey(padre)) {
				
				categoriaPadre = mapCategoria.get(padre);
				categoria = new Categoria(nome, categoriaPadre);
				categoriaPadre.setSottocategorie(categoria);
				mapCategoria.put(padre, categoriaPadre);
			}
			//METODO EQUIVALENTE
			/**for(Entry<String, Categoria> entry : mapCategoria.entrySet()) {
				//if(entry.getValue().getNome().equals(padre)) {
					categoriaPadre = entry.getValue();
					categoria = new Categoria(nome, categoriaPadre);
					categoriaPadre.setSottocategorie(categoria);
					//break;
				}*/
		}
		if(checkCategoria(categoria)) {
				
			// Inserimento della categoria nel DB			
			mapCategoria.put(nome , categoria);
			
			// Scrittura e chiusura del db
			db.commit();
			db.close();
			stampa= "aggiunta finita con successo";
		} 
		else stampa= "categoria gia' presente";
		
		return stampa;
	}
	
	//metodo per verificare che una categoria sia presente nel DB
	private static boolean checkCategoria(Categoria categoriaDaVerificare){
		DB db = DBConfig.getInstance();
		@SuppressWarnings("unchecked")
		BTreeMap<String, Categoria> mapCategoria =(BTreeMap<String, Categoria>) db.treeMap("mapCategorie").createOrOpen();
		
		
		if(!mapCategoria.isEmpty()) {
			for(Entry<String, Categoria> entry : mapCategoria.entrySet()) {
				if(entry.getValue().getNome().equalsIgnoreCase(categoriaDaVerificare.getNome())) 
					return false; //categoria presente nel DB	
			}
		}
		return true;
	}
	
	/*
	 * Inserisco Categorie di default
	 */
	public static void categorieDefault() {
		DB db = DBConfig.getInstance();
			
		@SuppressWarnings("unchecked")
		BTreeMap<String, Categoria> mapCategoria = (BTreeMap<String, Categoria>) db.treeMap("mapCategorie").createOrOpen();		
		ArrayList<Categoria> elencoCategorie = new ArrayList<Categoria>();	
		Categoria abbigliamento = new Categoria("Abbigliamento");
		//abbigliamento.setOggetti(new Oggetto("io","tuta","tuta blu",9.89,"12/12/2020", abbigliamento));
		Categoria casa = new Categoria("Casa");
		//casa.setOggetti(new Oggetto("io","lampada","proietta stelle",80.89,"12/12/2020", casa));
		Categoria elettronica = new Categoria("Elettronica");
		//elettronica.setOggetti(new Oggetto("io","lampadina","lampadina intelligente sony",19.89,"12/12/2020", elettronica));
		Categoria giardinaggio = new Categoria("Giardinaggio");
		//giardinaggio.setOggetti(new Oggetto("io","pala","molto leggera",69.89,"12/12/2020", giardinaggio));
		Categoria sport = new Categoria("Sport");
		//sport.setOggetti(new Oggetto("io","canoa","canoa gialla 2 posti",109.89,"12/12/2020", sport));
		
		elencoCategorie.add(abbigliamento);
		elencoCategorie.add(casa);
		elencoCategorie.add(elettronica);
		elencoCategorie.add(giardinaggio);
		elencoCategorie.add(sport);
		for(int i=0; i<elencoCategorie.size(); i++) 
			mapCategoria.put(elencoCategorie.get(i).getNome(), elencoCategorie.get(i));
		
		db.commit();
		db.close();
		
	}
	
	public static String rinominaCategoria(String nuovo, String vecchio) {
		
		DB db = DBConfig.getInstance();
		@SuppressWarnings("unchecked")
		BTreeMap<String, Categoria> mapCategoria = (BTreeMap<String, Categoria>) db.treeMap("mapCategorie").createOrOpen();	
		String stampa = "";
		
		if(nuovo.trim().isEmpty()) {
			stampa="Inserisci nuovo nome";
			return stampa;
		}
		
		/*for(Entry<String, Categoria> entry : mapCategoria.entrySet()) {
		  if(entry.getValue().getNome().equalsIgnoreCase(vecchio)) {
		  entry.getValue().setNome(nuovo);*/
		
		if(mapCategoria.containsKey(vecchio)) {
			Categoria categoriaVecchia = mapCategoria.get(vecchio);
			categoriaVecchia.setNome(nuovo);
			if(categoriaVecchia.getPadre()!=null) {
				Categoria padre = categoriaVecchia.getPadre();
				Categoria categoriaDaRimuovere = categoriaVecchia;
				categoriaDaRimuovere.setNome(vecchio);
				padre.getSottocategorie().remove(categoriaDaRimuovere);
				padre.setSottocategorie(categoriaVecchia);
				mapCategoria.put(padre.getNome(), padre);
			}
			 
			mapCategoria.put(nuovo, categoriaVecchia);
			mapCategoria.remove(vecchio);
			db.commit();
			db.close();
			stampa="Rinominata con successo";	
			
		}
		else stampa= "Seleziona categoria da rinominare";
		return stampa;
	}
	
	public static ArrayList<Categoria> getSottoCategorie(String padrecategorie){
		DB db = DBConfig.getInstance();
		@SuppressWarnings("unchecked")
		BTreeMap<String, Categoria> mapCategoria = (BTreeMap<String, Categoria>) db.treeMap("mapCategorie").createOrOpen();	
		return mapCategoria.get(padrecategorie).getSottocategorie();	 
		
	}
	
	public static ArrayList<Categoria> getCategorie(){
		DB db = DBConfig.getInstance();
		ArrayList<Categoria> elenco = new ArrayList<>();
		@SuppressWarnings("unchecked")
		BTreeMap<String, Categoria> mapCategoria = (BTreeMap<String, Categoria>) db.treeMap("mapCategorie").createOrOpen();	
		for( Categoria categoria : mapCategoria.values()) {
			if(categoria.getPadre()==null) {
				elenco.add(categoria);
			}
			
		}
		return elenco;
	}
}
