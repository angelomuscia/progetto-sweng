package myEbay.server;

import java.util.ArrayList;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import myEbay.client.GreetingService;
import myEbay.shared.Categoria;
import myEbay.shared.Domanda;
import myEbay.shared.Offerta;
import myEbay.shared.Oggetto;
import myEbay.shared.UtenteRegistrato;

/**
 * The server-side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class GreetingServiceImpl extends RemoteServiceServlet implements GreetingService {

	public void inizializzazione() {
		CategoriaDB.categorieDefault();
		Registration.regAdmin();
	}

	@Override
	public int doLogin (String username, String password) throws IllegalArgumentException {
		return Login.doLogin(username, password);
	}
	@Override
	public String doRegistration(ArrayList<String> infoutente) throws IllegalArgumentException{
		return Registration.doRegistration(infoutente);
	}
	@Override
	public String inserimentoAsta(String venditore, String nome, String categoria,
			double baseAsta, String scadenza, String descrizione ) throws IllegalArgumentException {
		return AstaDB.inserimentoAsta(venditore, nome, categoria, baseAsta, scadenza, descrizione);

	}
	@Override
	public ArrayList<Categoria> getSottoCategorie(String padrecategorie) throws IllegalArgumentException{
		return CategoriaDB.getSottoCategorie(padrecategorie);
	}

	@Override
	public ArrayList<Categoria> getCategorie() throws IllegalArgumentException {
		return CategoriaDB.getCategorie();
	}
	@Override
	public String rinominaCategoria(String nuovo, String vecchio) throws IllegalArgumentException {
		return CategoriaDB.rinominaCategoria(nuovo, vecchio);
	}

	@Override
	public String aggiungiCategoria(String padre, String nome) throws IllegalArgumentException {
		return CategoriaDB.aggiungiCategoria(padre, nome);
	}

	@Override
	public UtenteRegistrato getUtente(String nomeUtente) throws IllegalArgumentException {
		return Registration.getUtente(nomeUtente);
	}

	@Override
	public ArrayList<Oggetto> getOggetti() throws IllegalArgumentException {
		return AstaDB.getOggetti();
	}

	@Override
	public ArrayList<Oggetto> oggettiFiltrati(String categoria) throws IllegalArgumentException {
		return AstaDB.oggettiFiltrati(categoria);
	}

	@Override
	public ArrayList<Oggetto> myObjs(String utente) throws IllegalArgumentException {
		return AstaDB.myObjs(utente);
	}

	@Override
	public String doOfferta(double importo, String offerente, Oggetto oggetto) throws IllegalArgumentException {
		return OffertaDB.doOfferta(importo, offerente, oggetto);
	}

	@Override
	public ArrayList<Oggetto> mieOfferte(String utente) throws IllegalArgumentException {

		return OffertaDB.mieOfferte(utente);
	}

	@Override
	public Oggetto aggiungiDomanda(String testo, String mittente, Oggetto oggettoCommentato)
			throws IllegalArgumentException {
		return MessaggiDB.aggiungiDomanda(testo, mittente, oggettoCommentato);
	}

	@Override
	public Oggetto aggiungiRisposta(String testo, String venditore, Domanda domanda, Oggetto oggettoCommentato)
			throws IllegalArgumentException {
		return MessaggiDB.aggiungiRisposta(testo, venditore, domanda, oggettoCommentato);
	}

	@Override
	public String eliminaOggetto(Oggetto oggetto) throws IllegalArgumentException {

		return ServiziAdminDB.eliminaOggetto(oggetto);
	}

	@Override
	public Oggetto eliminaDomanda(Oggetto oggetto, Domanda domanda) throws IllegalArgumentException {

		return ServiziAdminDB.eliminaDomanda(oggetto, domanda);
	}

	@Override
	public Oggetto eliminaRisposta(Domanda domandaDiRiferimento, Oggetto oggetto) throws IllegalArgumentException {

		return ServiziAdminDB.eliminaRisposta(domandaDiRiferimento, oggetto);
	}

	@Override
	public Oggetto eliminaOfferta(Offerta offertaDaRimuovere, Oggetto oggetto) throws IllegalArgumentException {

		return ServiziAdminDB.eliminaOfferta(offertaDaRimuovere, oggetto);
	}

}
