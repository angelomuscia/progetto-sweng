package myEbay.server;

import java.util.ArrayList;

import org.mapdb.BTreeMap;
import org.mapdb.DB;

import myEbay.shared.Categoria;
import myEbay.shared.Domanda;
import myEbay.shared.Offerta;
import myEbay.shared.Oggetto;

public class ServiziAdminDB {

	
	public static Oggetto eliminaDomanda(Oggetto oggetto, Domanda domanda) {

		DB db = DBConfig.getInstance();
		@SuppressWarnings("unchecked")
		BTreeMap<String, Oggetto> mapOggetto = (BTreeMap<String, Oggetto>) db.treeMap("mapOggetti").createOrOpen();	
		@SuppressWarnings("unchecked")
		BTreeMap<String, Categoria> mapCategorie =(BTreeMap<String, Categoria>) db.treeMap("mapCategorie").createOrOpen();

		Oggetto obj = mapOggetto.get(oggetto.getKey());
		for(int i=0; i<obj.getDomande().size(); i++) {
			if(obj.getDomande().get(i).getTesto().equals(domanda.getTesto())) {
				obj.getDomande().remove(i);
				mapOggetto.put(obj.getKey(), obj);
				break;
			}
		}

		Categoria categ = mapCategorie.get(obj.getCategoria().getNome());

		ArrayList<Oggetto> listaobjs = categ.getOggetti();
		for(int i=0; i<listaobjs.size(); i++) {
			if(listaobjs.get(i).getKey().equals(obj.getKey())) {
				listaobjs.remove(i);
				listaobjs.add(i, obj);
				mapCategorie.put(categ.getNome(), categ);
				break;		
			}	
		}	
		db.commit();
		db.close();
		return obj;
	}

	
	public static Oggetto eliminaRisposta(Domanda domandaDiRiferimento, Oggetto oggetto) {

		DB db = DBConfig.getInstance();
		@SuppressWarnings("unchecked")
		BTreeMap<String, Oggetto> mapOggetto = (BTreeMap<String, Oggetto>) db.treeMap("mapOggetti").createOrOpen();	
		@SuppressWarnings("unchecked")
		BTreeMap<String, Categoria> mapCategorie =(BTreeMap<String, Categoria>) db.treeMap("mapCategorie").createOrOpen();

		Oggetto obj = mapOggetto.get(oggetto.getKey());
		for(int i=0; i<obj.getDomande().size(); i++) {
			if(obj.getDomande().get(i).getTesto().equals(domandaDiRiferimento.getTesto())) {
				Domanda d = obj.getDomande().get(i);
				if(!d.getRisposta().getTesto().equalsIgnoreCase("risposta non inserita")) {
					d.getRisposta().setTesto("");
					mapOggetto.put(obj.getKey(), obj);
					break;
				}
			}
		}

		Categoria categ = mapCategorie.get(obj.getCategoria().getNome());

		ArrayList<Oggetto> listaobjs = categ.getOggetti();
		for(int i=0; i<listaobjs.size(); i++) {
			if(listaobjs.get(i).getKey().equals(obj.getKey())) {
				listaobjs.remove(i);
				listaobjs.add(i, obj);
				mapCategorie.put(categ.getNome(), categ);
				break;		
			}	
		}	
		db.commit();
		db.close();
		return obj;
	}

	/*
	 * 	rimuove oggetti dalla mappa
	 * 	@Param Oggetto
	 * 	@return String
	 */
	
	public static String eliminaOggetto(Oggetto oggetto) {

		DB db = DBConfig.getInstance();
		@SuppressWarnings("unchecked")
		BTreeMap<String, Oggetto> mapOggetti = (BTreeMap<String, Oggetto>) db.treeMap("mapOggetti").createOrOpen();	
		@SuppressWarnings("unchecked")
		BTreeMap<String, Categoria> mapCategoria =(BTreeMap<String, Categoria>) db.treeMap("mapCategorie").createOrOpen();

		Oggetto objTORemove =mapOggetti.get(oggetto.getKey());
		Categoria categoria = mapCategoria.get(objTORemove.getCategoria().getNome());
		//toglie l'oggetto dall'arraylist di oggetti nella categoria corrispondente
		for(int i=0; i<categoria.getOggetti().size(); i++) {
			if(categoria.getOggetti().get(i).getKey().equals(objTORemove.getKey())) {
				categoria.getOggetti().remove(i);
				mapCategoria.put(categoria.getNome(), categoria);
				break;		
			}	
		}	

		mapOggetti.remove(objTORemove.getKey());	
		db.commit();
		db.close();
		return "Oggetto rimosso con successo!";
	}

	/*
	 * 	rimuove offerta dalla lista offerte 
	 * 	@Param Offerta, Oggetto
	 * 	@return oggetto
	 */
	public static Oggetto eliminaOfferta(Offerta offertaDaRimuovere, Oggetto oggetto ) {
		DB db = DBConfig.getInstance();
		@SuppressWarnings("unchecked")
		BTreeMap<String, Oggetto> mapOggetti = (BTreeMap<String, Oggetto>) db.treeMap("mapOggetti").createOrOpen();	
		@SuppressWarnings("unchecked")
		BTreeMap<String, Categoria> mapCategorie =(BTreeMap<String, Categoria>) db.treeMap("mapCategorie").createOrOpen();

		Oggetto obj =mapOggetti.get(oggetto.getKey());

		for(int i=0; i<obj.getOfferta().size(); i++) {
			Offerta offerta= obj.getOfferta().get(i);
			if(offerta.getOfferente().getUsername().equals(offertaDaRimuovere.getOfferente().getUsername())) {
				if(offerta.getImporto()==offertaDaRimuovere.getImporto()) {
					obj.getOfferta().remove(i);
					mapOggetti.put(obj.getKey(), obj);
					break;
				}
			}
		}
		Categoria categ = mapCategorie.get(obj.getCategoria().getNome());

		ArrayList<Oggetto> listaobjs = categ.getOggetti();
		for(int i=0; i<listaobjs.size(); i++) {
			if(listaobjs.get(i).getKey().equals(obj.getKey())) {
				listaobjs.remove(i);
				listaobjs.add(i, obj);
				mapCategorie.put(categ.getNome(), categ);
				break;		
			}	
		}	

		db.commit();
		db.close();
		return obj;	
	}	
}
