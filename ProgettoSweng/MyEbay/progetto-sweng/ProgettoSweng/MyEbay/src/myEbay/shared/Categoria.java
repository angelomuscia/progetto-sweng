package myEbay.shared;

import java.io.Serializable;
import java.util.ArrayList;

public class Categoria implements Serializable{

	/**
	 * Classe per la definizione di una Categoria
	 * 
	 * @author Angela Convertino, Elisa Andriani, Angelo Muscia Masuzzo
	 */
	private static final long serialVersionUID = 1L;
	private String nome;
	private Categoria padre;
	private ArrayList<Categoria> sottocategorie;
	
	
	//Costruttore 
	public Categoria() {}
	
	// Costruttore 
		// @param nome di tipo String
		public Categoria (String nome) {
			this.nome = nome;
			this.padre= null;
			sottocategorie = new ArrayList<Categoria>();
			
		}
		
	// Costruttore 
	// @param nome di tipo String
	// @param padre di tipo Categoria
	public Categoria (String nome, Categoria padre) {
		this.nome = nome;
		this.padre= padre;
		sottocategorie = new ArrayList<Categoria>();
		
	}
	
	
	//get Nome
	public String getNome() {
		return this.nome;
	}
	
	//set Nome
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	//get padre
	public Categoria getPadre() {
		return this.padre;
	}
	
	//set padre
	public void setPadre(Categoria padre) {
		this.padre = padre;
	}
	
	//get categoria figlia 
	public ArrayList<Categoria> getSottocategorie(){
		return sottocategorie;
	}
	
	//set categoria figlia
	public void setSottocategorie(Categoria categoria){
			this.sottocategorie.add(categoria);
	}
		
}
