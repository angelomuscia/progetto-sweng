package myEbay.shared;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;

public class Oggetto implements Serializable{

	/**
	 * Classe per la definizione di un UtenteRegistrato
	 * 
	 * @author Angela Convertino, Elisa Andriani, Angelo Muscia Masuzzo
	 */
	private static final long serialVersionUID = 1L;
	private String nome;
	private String descrizione;
	private double baseAsta;
	private String scadenza;
	private Categoria categoria;
	private UtenteRegistrato venditore;
	private LinkedList<Offerta> offerta;
	private LinkedList<Messaggio> messaggio;
	private String stato;

	/*
	 * Costruttore
	 * */
	public Oggetto() {
	}
	
	/*
	 * Costruttore
	 * @param venditore di tipo UtenteRegistrato
	 * @param nome di tipo String
	 * @param descrizione di tipo String
	 * @param baseAsta di tipo double
	 * @param scadenza di tipo String
	 * @param categoria di tipo Categoria
	 */
	public Oggetto(UtenteRegistrato venditore, String nome, String descrizione, double baseAsta, String scadenza,
			Categoria categoria) {
		this.venditore = venditore;
		this.nome = nome;
		this.descrizione = descrizione;
		this.baseAsta= baseAsta;
		this.scadenza = scadenza;
		this.categoria = categoria;
	}

	// GET nome, descrizione, base asta, scadenza asta, categoria, venditore, offerta, messaggio, stato
	public String getNome() {
		return nome;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public double getBaseAsta() {
		return baseAsta;
	}

	public String getScadenza() {
		return scadenza;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public UtenteRegistrato getVenditore() {
		return venditore;
	}

	public LinkedList<Offerta> getOfferta() {
		return offerta;
	}

	public LinkedList<Messaggio> getMessaggio() {
		return messaggio;
	}

	public String getStato() {
		return stato;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	// SET nome, descrizione, base asta, scadenza asta, categoria, venditore, offerta, messaggio, stato
	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public void setBaseAsta(double baseAsta) {
		this.baseAsta = baseAsta;
	}

	public void setScadenza(String scadenza) {
		this.scadenza = scadenza;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public void setVenditore(UtenteRegistrato venditore) {
		this.venditore = venditore;
	}

	public void setOfferta(LinkedList<Offerta> offerta) {
		this.offerta = offerta;
	}

	public void setMessaggio(LinkedList<Messaggio> messaggio) {
		this.messaggio = messaggio;
	}

	public void setStato(String stato) {
		this.stato = stato;
	}
	
}
