package myEbay.shared;

public class Amministratore extends Utente{

	private static final long serialVersionUID = 1L;
	
	
	//costruttore
	public Amministratore(String username, String password, String email) {
		super(username, password, email);
	}
	
	//get
	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public String getEmail() {
		return email;
	}

    // metodi
	public void eliminaMessaggio(Messaggio messaggio){};
	
	public void eliminaOfferta(Offerta offerta){};
	
	public void eliminaOggetto(Oggetto oggetto) {};
	
	public Categoria aggiungiCategoria(String nomeCategoria) {
		return null;
	};
	
	public void eliminaCategoria(String nomeCategoria) {};
	public void rinominaCategoria(String nomeCategoria, Categoria categoria) {};
	

}
