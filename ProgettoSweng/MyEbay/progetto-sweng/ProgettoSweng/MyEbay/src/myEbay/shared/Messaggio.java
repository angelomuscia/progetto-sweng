package myEbay.shared;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedList;

public class Messaggio implements Serializable {
	
	/**
	 * mittente e timestamp chiavi primarie
	 */
	private static final long serialVersionUID = 1L;
	private String testo;
	private UtenteRegistrato mittente;
	private LinkedList<Messaggio> risposta;
	private Timestamp timestamp;
	
	
	//costruttore senza parametri
	public Messaggio() {}
	
	//costruttore con parametri
	public Messaggio(String testo, UtenteRegistrato mittente, Timestamp timestamp) {
		this.testo = testo;
		this.mittente = mittente;
		this.timestamp = new Timestamp(System.currentTimeMillis());
	}
	
	public String getTesto() {
		return testo;
	}
	
	public void setTesto(String testo) {
		this.testo = testo;
	}
	
	public UtenteRegistrato getMittente() {
		return mittente;
	}
	
	public void setMittente(UtenteRegistrato mittente) {
		this.mittente = mittente;
	}
	
	public Timestamp getTimestamp() {
		return timestamp;
	}
	
	
	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = new Timestamp(System.currentTimeMillis());
	}

	public LinkedList<Messaggio> getRisposta() {
		return risposta;
	}

	public void setRisposta(LinkedList<Messaggio> risposta) {
		this.risposta = risposta;
		
	}
	
	
}
