package myEbay.shared;

import java.io.Serializable;

public class Utente implements Serializable{

	static final long serialVersionUID = 1L;

	protected String username;
	protected String password;
	protected String email;
	
	public Utente(String username, String password, String email) {
		this.username=username;
		this.password=password;
		this.email=email;
	}
	
	//get
	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public String getEmail() {
		return email;
	}

	/*public void mettereInVendita(){
		
	};
	
	public void scriviMessaggio(){
		
	};
	
	public void fareOfferta(){
		
	};*/
	
}
