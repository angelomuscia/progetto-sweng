package myEbay.server;

import static org.junit.Assert.assertArrayEquals;

import java.text.DateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Map.Entry;

import org.apache.james.mime4j.field.datetime.DateTime;
import org.mapdb.BTreeMap;
import org.mapdb.DB;

import com.google.gwt.i18n.shared.DateTimeFormat;

import myEbay.shared.Categoria;
import myEbay.shared.Offerta;
import myEbay.shared.Oggetto;
import myEbay.shared.UtenteRegistrato;

public class OffertaDB {

	/**
	 * Metodo che aggiunge un'offerta al DB se non ancora esistente
	 * @param offerta
	 * @return stringa stampa con avviso
	 */
	@SuppressWarnings({ "unused", "unchecked" })
	private static String doOfferta(Offerta offertaDaInserire, Oggetto oggettoConsiderato) {
	    DB db = DBConfig.getInstance();
		BTreeMap<Oggetto, Offerta> mapOfferta;
		
		String stampa=null;
		//controllo se esiste l'offerente nell'offerta e se l'importo � un prezzo maggiore di 0
		if((offertaDaInserire.getOfferente()!=null && offertaDaInserire.getOfferente() instanceof UtenteRegistrato)|| offertaDaInserire.getImporto()>0.0){
			if(checkOfferta(offertaDaInserire, oggettoConsiderato)) {
				mapOfferta = (BTreeMap<Oggetto, Offerta>) db.treeMap("mapOfferta").createOrOpen();

				// Inserimento della offerta nel DB
				mapOfferta.put(oggettoConsiderato, offertaDaInserire);
				// Scrittura e chiusura del db
				db.commit();
				db.close();
			    stampa= "aggiunta offerta finita con successo";
			}
			else 
				stampa= "offerta gia' presente";
		}
		else stampa= "inserisci offerta";
		
		return stampa;
	}
	
	
	
	//metodo per verificare che un'offerta sia presente nel DB
	public static boolean checkOfferta(Offerta offertaDaVerificare, Oggetto oggettoConsiderato){
		DB db = DBConfig.getInstance();
		@SuppressWarnings("unchecked")
		BTreeMap<Oggetto, Offerta> mapOfferta =(BTreeMap<Oggetto, Offerta>) db.treeMap("mapOfferta").createOrOpen();
		boolean risposta=false;
		
		if(mapOfferta.isEmpty()) 
			risposta=false;
		else {
			for(Entry<Oggetto, Offerta> entry : mapOfferta.entrySet()) {
				if(entry.getKey().equals(oggettoConsiderato))
					if(entry.getValue().equals(offertaDaVerificare)) 
						risposta=true; //offerta presente nel DB
			}
		}
		return risposta;
	}
	
	// STATO OGGETTO PU� ESSERE: 
	/* asta in corso
	 * asta conclusa e oggetto aggiudicato da�
	 * asta conclusa e oggetto non aggiudicato */
	// STATO OFFERTA PU� ESSERE: 
	/* asta chiusa e oggetto aggiudicato
	 * asta chiusa e oggetto non aggiudicato
	 * asta in corso e offerta migliore
	 * asta in corso e offerta superata
	 */
	
	//metodo per cambiare stato all'offerta presa in considerazione
	@SuppressWarnings("unchecked")
	public static void cambiaStato(Offerta offertaConsiderata, Oggetto oggettoConsiderato){	
		
		Date oggi = new Date();
		
		Date astaScadenza = DateFormat.forPattern("dd/MM/yyyy HH:mm:ss");
		DateTime scadenzaAsta = astaScadenza.parseDateTime(oggettoConsiderato.getScadenza());
		DB db = DBConfig.getInstance();
		@SuppressWarnings("unchecked")
		BTreeMap<Oggetto, Offerta> mapOfferta =(BTreeMap<Oggetto, Offerta>) db.treeMap("mapOfferta").createOrOpen();
		// controllo se MapOfferta � piena cerco le offerte per un determinato oggetto
		if(mapOfferta.isEmpty()==false) {
			for(Entry<Oggetto, Offerta> entry : mapOfferta.entrySet()) {
				if(entry.getKey().equals(oggettoConsiderato)) {
					// se scadenza � uguale alla data attuale allora setto stato asta a conclusa
					if((Date)entry.getKey().getScadenza()>oggi) {
						if(entry.getValue().getImporto()>=offertaConsiderata.getImporto()) {
							entry.getKey().setStato("asta conclusa e oggetto aggiudicato da");
							entry.getValue().setStato("asta chiusa e oggetto aggiudicato");
							offertaConsiderata.setStato("asta conclusa e oggetto non aggiudicato"); 
						}else {
							offertaConsiderata.setStato("asta conclusa e oggetto aggiudicato da ");
							entry.getValue().setStato("asta chiusa e oggetto non aggiudicato");
							entry.getKey().setStato("asta conclusa e oggetto non aggiudicato");
						}
					}else {
						entry.getKey().setStato("asta in corso");
						if(entry.getValue().getImporto()>offertaConsiderata.getImporto()) 
							entry.getValue().setStato("asta in corso e offerta");
						else
							entry.getValue().setStato("asta in corso e offerta superata");
					}
				}
			}
		}
	}
	
}
	

