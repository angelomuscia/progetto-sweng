package myEbay.server;
import java.util.ArrayList;
import java.util.Map.Entry;

import org.mapdb.BTreeMap;
import org.mapdb.DB;

import myEbay.shared.Amministratore;
import myEbay.shared.Categoria;
import myEbay.shared.Utente;
public class CategoriaDB {
	
	/**
	 * Metodo che aggiunge una categoria al DB se non ancora esistente
	 * @param categoria
	 * @return stringa stampa con avviso
	 */
	@SuppressWarnings({ "unused", "unchecked" })
	
	public static String aggiungiCategoria(Categoria categoria) {
	    DB db = DBConfig.getInstance();
		BTreeMap<String, Categoria> mapCategoria;
		
		String stampa=null;
		//controllo se il nome della categoria � vuoto o tolgo eventuali spaziature per fare il controllo
		if(categoria.getNome().isEmpty() || categoria.getNome().trim().length()<=0) {
			if(checkCategoria(categoria)) {
				mapCategoria = (BTreeMap<String, Categoria>) db.treeMap("mapCategoria").createOrOpen();

				// Inserimento della categoria nel DB
				mapCategoria.put( categoria.getNome(), categoria);
				// Scrittura e chiusura del db
				db.commit();
				db.close();
			    stampa= "aggiunta finita con successo";
			}
			else 
				stampa= "categoria gia' presente";
		}
		else stampa= "inserisci categoria";
		
		return stampa;
	}
	
	//metodo per verificare che una categoria sia presente nel DB
	public static boolean checkCategoria(Categoria categoriaDaVerificare){
		DB db = DBConfig.getInstance();
		@SuppressWarnings("unchecked")
		BTreeMap<String, Categoria> mapCategoria =(BTreeMap<String, Categoria>) db.treeMap("mapCategoria").createOrOpen();
		boolean risposta=false;
		
		if(mapCategoria.isEmpty()) 
			risposta=false;
		else {
			for(Entry<String, Categoria> entry : mapCategoria.entrySet()) {
				if(entry.getValue().getNome().equalsIgnoreCase(categoriaDaVerificare.getNome())) 
					risposta=true; //categoria presente nel DB
			}
		}
		return risposta;
	}
	
	/*
	 * Inserisco Categorie di default
	 */
	public static void categorieDefault() {
		DB db = DBConfig.getInstance();
		@SuppressWarnings("unchecked")
		BTreeMap<String, Categoria> mapCategoria = (BTreeMap<String, Categoria>) db.treeMap("mapCategoria").createOrOpen();		
		ArrayList<Categoria> elencoCategorie = new ArrayList<Categoria>();	
		elencoCategorie.add(new Categoria("Abbigliamento"));
		elencoCategorie.add(new Categoria("Casa"));
		elencoCategorie.add(new Categoria("Elettronica"));
		elencoCategorie.add(new Categoria("Giardinaggio"));
		elencoCategorie.add(new Categoria("Sport"));
		for(int i=0; i<elencoCategorie.size(); i++) 
			mapCategoria.put(elencoCategorie.get(i).getNome(), elencoCategorie.get(i));
		db.commit();
		db.close();
		
	}
	
	
}
