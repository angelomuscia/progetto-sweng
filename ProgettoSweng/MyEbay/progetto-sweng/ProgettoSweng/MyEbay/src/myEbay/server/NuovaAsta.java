package myEbay.server;
import org.mapdb.BTreeMap;
import org.mapdb.DB;

import java.util.Map.Entry;
import java.util.UUID;
import myEbay.shared.Oggetto;

public class NuovaAsta {

		//metodo inserimento oggetto in asta
	@SuppressWarnings("unchecked")
	public static void inserimentoAsta(Oggetto oggettoDaInserire) {
		
		DB db = DBConfig.getInstance();
		String id = UUID.randomUUID().toString();
		BTreeMap<String, Oggetto> mapCategoria = (BTreeMap<String, Oggetto>) db.treeMap("mapCategoria").createOrOpen();	
		mapCategoria.put(id,oggettoDaInserire);
		db.commit();
		db.close();
	} 
	
	
	
	//metodo che sposta oggetti nella mappa dei venduti o degli oggetti in vendita
	@SuppressWarnings("unchecked")
	public static void venduto(){	
		DB db = DBConfig.getInstance();
		@SuppressWarnings("unchecked")
		BTreeMap<String, Oggetto> mapCategoria = (BTreeMap<String, Oggetto>) db.treeMap("mapCategoria").createOrOpen();	
		BTreeMap<String, Oggetto> mapVenduti = (BTreeMap<String, Oggetto>) db.treeMap("mapVenduti").createOrOpen();
		BTreeMap<String, Oggetto> mapInvenduti = (BTreeMap<String, Oggetto>) db.treeMap("mapInvenduti").createOrOpen();
		// controllo lo stato di ogni oggetto
		if(mapCategoria.isEmpty()==false) {
			for(Entry<String, Oggetto> entry : mapCategoria.entrySet() ) {
				// se stato oggetto contiene la frase asta chiusa lo sposto nel hash venduta o invenduta in base alla destinazione dell'oggetto
				if(entry.getValue().getStato().contains("asta chiusa")){
					if(entry.getValue().getStato().contains("non")){					
						mapInvenduti.put(entry.getKey(),entry.getValue());
						mapCategoria.remove(entry.getKey());
					}else {
						mapVenduti.put(entry.getKey(),entry.getValue());
						mapCategoria.remove(entry.getKey());
					}
				}
			}
		}
	}
	
}

