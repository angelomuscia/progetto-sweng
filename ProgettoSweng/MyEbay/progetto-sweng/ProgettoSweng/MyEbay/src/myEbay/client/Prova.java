package myEbay.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class Prova extends Composite {

	private static ProvaUiBinder uiBinder = GWT.create(ProvaUiBinder.class);

	interface ProvaUiBinder extends UiBinder<Widget, Prova> {
	}

	public Prova() {
		initWidget(uiBinder.createAndBindUi(this));
	}

}
