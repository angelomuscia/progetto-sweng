package myEbay;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import myEbay.server.CategoriaDB;
import myEbay.server.Login;
import myEbay.server.Registration;
import myEbay.shared.Categoria;
/**
 * Classe di test
 *
 * @author Andriani Elisa
 */
class TestClass {
	
	@Test
	/**
	 * Test per il Login
	 */
	@DisplayName("Test di login")
	void testLogin() {
		
		ArrayList<String> dati= new ArrayList<String>();
		for(int i=0; i<11; i++) {
			dati.add("utenteinfo");
		}
		//utente non trovato
		assertEquals(0, Login.doLogin("utenteprova", "pswprova") );
		//utente ha effettuato il login
		Registration.doRegistration(dati);
		assertEquals(2, Login.doLogin("utenteinfo", "utenteinfo") );
		//admin ha effettuato il login
		Registration.regAdmin();
		assertEquals(1, Login.doLogin("admin", "admin") );
		
	}
	
	@Test
	/**
	 * Test per la Registrazione
	 */
	@DisplayName("Test di registrazione")
	void testRegistrazione() {
		
		ArrayList<String> dati= new ArrayList<String>();
		for(int i=0; i<11; i++) {
			dati.add("Utenteinfo");
		}
		assertEquals("Registazione completata con successo", Registration.doRegistration(dati) );
		assertEquals("Username gi� presente",  Registration.doRegistration(dati));
	}
	
	@Test
	/**
	 * Test per l'inserimento di una categoria.
	 */
	@DisplayName("Test categoria")
	void testAggiungiCategoria() {
		
		//successo
		Categoria categoria = new Categoria ("Giocattoli");
		assertEquals("aggiunta finita con successo", CategoriaDB.aggiungiCategoria(categoria));
		
		//categoria gi� esistente
		CategoriaDB.categorieDefault();
		Categoria categoriaS = new Categoria ("Sport");
		assertEquals("categoria gia' presente",CategoriaDB.aggiungiCategoria(categoriaS));
		
		//caso vuoto
		Categoria categoriaNull = new Categoria ("        ");
		assertEquals("inserisci categoria",CategoriaDB.aggiungiCategoria(categoriaNull));
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}