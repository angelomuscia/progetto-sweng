 Questo è il repository del Progetto A.A. 2019/2020 di Ingegneria del Software, svolto da:
* Angela Convertino, Elisa Andriani e Angelo Muscia Masuzzo*.

### Consiste in un sito web di Aste online. L’applicazione permette ad un utente di:###
* Registrarsi al sito
* Mettere in vendita un oggetto
* Fare un’offerta per un oggetto messo in vendita
* Chiedere informazioni al venditore dell’oggetto

Nella repository sono presenti i codici sorgenti che implementano la versione 1.1 dell'applicazione web.